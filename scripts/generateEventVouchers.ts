import { ethers } from "ethers";


async function generateVouchers(numberOfVouchers: number) {
    for (let i=0; i< numberOfVouchers; i++) {
        const wallet = ethers.Wallet.createRandom();
        const mnemonic = wallet.mnemonic.phrase;
       
        console.log(mnemonic);
    }
}

generateVouchers(25).then(() => console.log("All Vouchers Generated")).catch((error) => console.error("Error Recorded: ", error))