// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { BigNumber } from "@ethersproject/bignumber";
import "hardhat-gas-reporter";
import hre from "hardhat";

type EVM_ADDRESS = string;

interface TokenDeposits {
  address: EVM_ADDRESS,
  ammount: BigNumber
}

interface LLamaContractArgs {
  GovernanceToken: TokenDeposits[];
  LlamaToken: {
    LlamaCurrency: TokenDeposits[];
    LlamaLeader: EVM_ADDRESS;
  }
}

interface LlamaContracts {
  LlamaShares: EVM_ADDRESS;
  GovernanceContract: EVM_ADDRESS;
  LlamaToken: EVM_ADDRESS;
  LLamaDirectory: EVM_ADDRESS;
}

async function deployLlamas(args: LLamaContractArgs): Promise<LlamaContracts> {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');
  let fuelGuage = 0;
  const LlamaShares = await hre.ethers.getContractFactory("LlamaShares");
  const [sharesAccounts, sharesAmmounts] = SplitTokenDeposits(args.GovernanceToken);
  const shares = await LlamaShares.deploy(sharesAccounts, sharesAmmounts);
  console.log(`LlamaShares Governance token has been depolyed to blockchain, contract address: ${shares.address}`);

  const GovernanceContract = await hre.ethers.getContractFactory("LlamaGovernor");
  const governance = await GovernanceContract.deploy(shares.address);
  console.log(`LlamaTipping Governance Contract has been deployed to blockchain, contract address: ${governance.address}`);

  const LlamaToken = await hre.ethers.getContractFactory("LlamaToken");
  const [tokenAccounts, tokenAmmounts] = SplitTokenDeposits(args.LlamaToken.LlamaCurrency);
  const llama = await LlamaToken.deploy(tokenAccounts, tokenAmmounts, args.LlamaToken.LlamaLeader, governance.address);
  console.log(`LlamaToken has been deployed to blockchain, contract address: ${llama.address}`);

  const LlamaDirectory = await hre.ethers.getContractFactory("LlamaDirectory");
  const directory = await hre.upgrades.deployProxy(LlamaDirectory, [shares.address, governance.address, llama.address]);
  console.log(`LlamaDirectory Contract has been deployed to blockchain, contract address: ${directory.address}`);


  return {
    GovernanceContract: governance.address,
    LLamaDirectory: directory.address,
    LlamaShares: shares.address,
    LlamaToken: llama.address
  }
}

function SplitTokenDeposits(deposits: TokenDeposits[]): [EVM_ADDRESS[], BigNumber[]] {
  const addresses: EVM_ADDRESS[] = [];
  const ammounts: BigNumber[] = [];
  for (const deposit of deposits) {
    addresses.push(deposit.address);
    ammounts.push(deposit.ammount);
  }
  return [addresses, ammounts];
}

async function main(production: boolean) {
  const accounts = await hre.ethers.getSigners();
  const BRANDON_ROSPTEN_SOFT = "0xBbD71B8Bf936EA07C5094DE9d6fB5F5C68FBBC5b";
  const llamaArgs: LLamaContractArgs = production ?
    {
      GovernanceToken: [
        {
          address: accounts[0].address,
          ammount: BigNumber.from("33000000000000000000")
        },
        {
          address: accounts[1].address,
          ammount: BigNumber.from("33000000000000000000")
        },
        {
          address: accounts[2].address,
          ammount: BigNumber.from("34000000000000000000")
        }
      ],
      LlamaToken: {
        LlamaCurrency: [
          {
            address: accounts[0].address,
            ammount: BigNumber.from("333333")
          },
          {
            address: accounts[1].address,
            ammount: BigNumber.from("333333")
          },
          {
            address: accounts[2].address,
            ammount: BigNumber.from("333334")
          }
        ],
        LlamaLeader: accounts[0].address
      }
    } :
    {
      GovernanceToken: [
        {
          address: BRANDON_ROSPTEN_SOFT,
          ammount: BigNumber.from("100000000000000000000")
        }
      ],
      LlamaToken: {
        LlamaCurrency: [
          {
            address: "33000000000000000000",
            ammount: BigNumber.from("1000000")
          },
        ],
        LlamaLeader: BRANDON_ROSPTEN_SOFT
      }
    };
  await deployLlamas(llamaArgs);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main(false)
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
