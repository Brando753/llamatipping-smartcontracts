import { expect } from "chai";
import { ethers } from "hardhat";
import { LlamaToken, LlamaToken__factory } from "../typechain-types";
import { BigNumber } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import hh from "hardhat";
import {
  CompilerOutputContractWithSlots,
  NULL_ADDRESS,
  StorageSlot,
} from "./common";

describe("LlamaTokens smart contract", () => {
  let userA: SignerWithAddress,
    userB: SignerWithAddress,
    userC: SignerWithAddress,
    userD: SignerWithAddress;
  let purchaseContract: SignerWithAddress,
    governanceContract: SignerWithAddress;
  let llamaTokenFactory: LlamaToken__factory;
  let llamaToken: LlamaToken;

  async function getPrivateValue(name: string): Promise<string> {
    const buildArtifacts = await hh.artifacts.getBuildInfo(
      "contracts/LlamaToken.sol:LlamaToken"
    );
    const storageLayout = (
      buildArtifacts?.output.contracts["contracts/LlamaToken.sol"]
        .LlamaToken as CompilerOutputContractWithSlots
    ).storageLayout;
    const tokenIdSlot = storageLayout.storage.find(
      (entry) => entry.label === name
    ) as StorageSlot;
    const value = await ethers.provider.getStorageAt(
      llamaToken.address,
      Number(tokenIdSlot.slot)
    );
    return value;
  }
  describe("Deploying the constructor", () => {
    beforeEach(async () => {
      [userA, userB, userC, userD, purchaseContract, governanceContract] =
        await ethers.getSigners();
      llamaTokenFactory = await ethers.getContractFactory("LlamaToken");
    });
    it("should require the length of addresses and values to match", async () => {
      const llamaToken = llamaTokenFactory.deploy(
        [userA.address, userB.address, userC.address],
        [userA.address],
        [1000, 1000],
        userA.address,
        governanceContract.address,
        purchaseContract.address
      );
      await expect(llamaToken).to.be.revertedWith(
        "Quantity and Addresses arrays not matching"
      );
    });
    it("should prevent minting more then one hundred million llamas", async () => {
      const llamaToken = llamaTokenFactory.deploy(
        [userA.address, userB.address, userC.address],
        [userA.address],
        [10_000_000, 50_000_000, 50_000_000],
        userA.address,
        governanceContract.address,
        purchaseContract.address
      );
      await expect(llamaToken).to.be.revertedWith(
        "Supply of Tipped Llamas Is Not One Million Llamas or less"
      );
    });
    it("Should only mint a single Magic Conch Shell", async () => {
      llamaToken = await llamaTokenFactory.deploy(
        [userA.address, userB.address, userC.address],
        [userA.address, userB.address, userC.address],
        [1_000, 1_000, 998_000],
        userA.address,
        governanceContract.address,
        purchaseContract.address
      );
      expect(
        await llamaToken.totalSupply(await llamaToken.MAGIC_CONCH_SHELL())
      ).to.equal("1");
    });
    it("Should mint the correct initial balances", async () => {
      llamaToken = await llamaTokenFactory.deploy(
        [userA.address, userB.address, userC.address],
        [userA.address, userC.address, userD.address],
        [9_185, 127, 526_328],
        userA.address,
        governanceContract.address,
        purchaseContract.address
      );
      // User Balances of Badges / NFT's
      expect(
        await llamaToken.balanceOf(
          userA.address,
          await llamaToken.MAGIC_CONCH_SHELL()
        )
      ).to.equal("1");
      expect(
        await llamaToken.balanceOf(
          userB.address,
          await llamaToken.MAGIC_CONCH_SHELL()
        )
      ).to.equal("0");
      expect(
        await llamaToken.balanceOf(
          userC.address,
          await llamaToken.MAGIC_CONCH_SHELL()
        )
      ).to.equal("0");
      expect(
        await llamaToken.balanceOf(
          userD.address,
          await llamaToken.MAGIC_CONCH_SHELL()
        )
      ).to.equal("0");

      expect(
        await llamaToken.balanceOf(
          userA.address,
          await llamaToken.GLITCHY_LLAMA()
        )
      ).to.equal("1");
      expect(
        await llamaToken.balanceOf(
          userB.address,
          await llamaToken.GLITCHY_LLAMA()
        )
      ).to.equal("0");
      expect(
        await llamaToken.balanceOf(
          userC.address,
          await llamaToken.GLITCHY_LLAMA()
        )
      ).to.equal("1");
      expect(
        await llamaToken.balanceOf(
          userD.address,
          await llamaToken.GLITCHY_LLAMA()
        )
      ).to.equal("1");

      expect(
        await llamaToken.balanceOf(userA.address, await llamaToken.OLD_LLAMA())
      ).to.equal("1");
      expect(
        await llamaToken.balanceOf(userB.address, await llamaToken.OLD_LLAMA())
      ).to.equal("1");
      expect(
        await llamaToken.balanceOf(userC.address, await llamaToken.OLD_LLAMA())
      ).to.equal("1");
      expect(
        await llamaToken.balanceOf(userD.address, await llamaToken.OLD_LLAMA())
      ).to.equal("0");

      // Total Supply of Badges / NFT's
      expect(
        await llamaToken.totalSupply(await llamaToken.MAGIC_CONCH_SHELL())
      ).to.equal("1");

      // This value is different for Mumbai testnet
      // expect(await llamaToken.totalSupply(await llamaToken.GLITCHY_LLAMA())).to.equal("2");
      expect(
        await llamaToken.totalSupply(await llamaToken.GLITCHY_LLAMA())
      ).to.equal("5003");
      // Remove this as will for mainnet
      expect(
        await llamaToken.balanceOf(
          purchaseContract.address,
          await llamaToken.GLITCHY_LLAMA()
        )
      ).to.equal("5000");

      expect(
        await llamaToken.totalSupply(await llamaToken.OLD_LLAMA())
      ).to.equal("3");

      // User Balances of TippedLlama Token
      expect(
        await llamaToken.balanceOf(
          userA.address,
          await llamaToken.TIPPED_LLAMAS()
        )
      ).to.equal("9185");
      expect(
        await llamaToken.balanceOf(
          userB.address,
          await llamaToken.TIPPED_LLAMAS()
        )
      ).to.equal("127");
      expect(
        await llamaToken.balanceOf(
          userC.address,
          await llamaToken.TIPPED_LLAMAS()
        )
      ).to.equal("526328");

      // Total Supply of TippedLlama Token
      expect(
        await llamaToken.totalSupply(await llamaToken.TIPPED_LLAMAS())
      ).to.equal("535640");
    });
    it("Should setup the correct roles", async () => {
      llamaToken = await llamaTokenFactory.deploy(
        [userA.address, userB.address, userC.address],
        [userA.address, userC.address, userD.address],
        [9_185, 127, 526_328],
        userA.address,
        governanceContract.address,
        purchaseContract.address
      );
      const roles = await Promise.all([
        llamaToken.DEFAULT_ADMIN_ROLE(),
        llamaToken.LLAMA_MINTER_ROLE(),
        llamaToken.MINTER_ROLE(),
        llamaToken.URI_ROLE(),
      ]);
      const userAddresses = [
        userA.address,
        userB.address,
        userC.address,
        userD.address,
      ];
      for (const role of roles) {
        for (const user of userAddresses) {
          // None of the users should be assigned to any roles
          expect(await llamaToken.hasRole(role, user)).to.equal(false);
        }
        // Governance contract should have ALL the roles
        expect(
          await llamaToken.hasRole(role, governanceContract.address)
        ).to.equal(true);
      }
      // The purchase contract should have the LLama Minter Role
      expect(
        await llamaToken.hasRole(
          await llamaToken.LLAMA_MINTER_ROLE(),
          purchaseContract.address
        )
      ).to.equal(true);

      // Verify the correct number of roles have been setup
      expect(
        await llamaToken.getRoleMemberCount(
          await llamaToken.DEFAULT_ADMIN_ROLE()
        )
      ).to.equal("1");
      expect(
        await llamaToken.getRoleMemberCount(await llamaToken.URI_ROLE())
      ).to.equal("1");
      expect(
        await llamaToken.getRoleMemberCount(await llamaToken.MINTER_ROLE())
      ).to.equal("1");
      expect(
        await llamaToken.getRoleMemberCount(
          await llamaToken.LLAMA_MINTER_ROLE()
        )
      ).to.equal("2");
    });
    it("should automatically increment the counter to the correct value", async () => {
      llamaToken = await llamaTokenFactory.deploy(
        [userA.address, userB.address, userC.address],
        [userA.address, userC.address, userD.address],
        [9_185, 127, 526_328],
        userA.address,
        governanceContract.address,
        purchaseContract.address
      );
      expect(await llamaToken.getNextTokenId()).to.equal(4);
    });
  });
  describe("LlamaToken contract", () => {
    beforeEach(async () => {
      [userA, userB, userC, userD, purchaseContract, governanceContract] =
        await ethers.getSigners();
      llamaTokenFactory = await ethers.getContractFactory("LlamaToken");
      llamaToken = await llamaTokenFactory.deploy(
        [],
        [userA.address, userC.address, userD.address],
        [],
        userA.address,
        governanceContract.address,
        purchaseContract.address
      );
    });
    describe("Magic Conch Shell transfers", () => {
      let tokenId: BigNumber;
      beforeEach(async () => {
        tokenId = await llamaToken.MAGIC_CONCH_SHELL();
      });
      it("should not let others call changeConchOwnership", async () => {
        expect(
          await llamaToken.balanceOf(
            userA.address,
            await llamaToken.MAGIC_CONCH_SHELL()
          )
        ).to.equal(1);
        await expect(
          llamaToken.connect(userA).changeConchOwnership(userB.address)
        ).to.be.revertedWith("caller is not governance contract");
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(1);
        expect(await llamaToken.balanceOf(userB.address, tokenId)).to.equal(0);
      });
      it("should allow the governance contract to call changeConchOwnership", async () => {
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(1);
        await llamaToken
          .connect(governanceContract)
          .changeConchOwnership(userB.address);
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        expect(await llamaToken.balanceOf(userB.address, tokenId)).to.equal(1);
      });
      it("should allow the governance contract to call changeConchOwnership even if it was transferred to someone else", async () => {
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(1);
        await llamaToken
          .connect(userA)
          .safeTransferFrom(userA.address, userB.address, tokenId, 1, []);
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        expect(await llamaToken.balanceOf(userB.address, tokenId)).to.equal(1);

        await llamaToken
          .connect(governanceContract)
          .changeConchOwnership(userC.address);
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        expect(await llamaToken.balanceOf(userB.address, tokenId)).to.equal(0);
        expect(await llamaToken.balanceOf(userC.address, tokenId)).to.equal(1);
      });
      it("should allow the governance contract to call changeConchOwnership even if it was batch transferred to someone else", async () => {
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(1);
        await llamaToken
          .connect(userA)
          .safeBatchTransferFrom(
            userA.address,
            userB.address,
            [tokenId],
            [1],
            []
          );
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        expect(await llamaToken.balanceOf(userB.address, tokenId)).to.equal(1);

        await llamaToken
          .connect(governanceContract)
          .changeConchOwnership(userC.address);
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        expect(await llamaToken.balanceOf(userB.address, tokenId)).to.equal(0);
        expect(await llamaToken.balanceOf(userC.address, tokenId)).to.equal(1);
      });
    });
    describe("Minting of Tipped Llamas", () => {
      let tokenId: BigNumber;
      let tokenCap: BigNumber;
      beforeEach(async () => {
        const tokenIdPromise = llamaToken.TIPPED_LLAMAS();
        const tokenCapPromise = llamaToken.TIPPED_LLAMAS_CAP();
        [tokenId, tokenCap] = await Promise.all([
          tokenIdPromise,
          tokenCapPromise,
        ]);
      });
      it("should not let normal users call mintTippedLlamas", async () => {
        await expect(
          llamaToken.connect(userA).mintTippedLLamas(userA.address, 100_000)
        ).to.be.revertedWith("Caller is not llama minter");
        await expect(
          llamaToken.connect(userB).mintTippedLLamas(userA.address, 100_000_000)
        ).to.be.revertedWith("Caller is not llama minter");
        await expect(
          llamaToken.connect(userC).mintTippedLLamas(userB.address, 10_000)
        ).to.be.revertedWith("Caller is not llama minter");
        await expect(
          llamaToken.connect(userD).mintTippedLLamas(userC.address, 90_000)
        ).to.be.revertedWith("Caller is not llama minter");
        await expect(
          llamaToken.connect(userD).mintTippedLLamas(userD.address, 100)
        ).to.be.revertedWith("Caller is not llama minter");
      });
      it("should let the governance contractor to mint new tokens", async () => {
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        await expect(
          llamaToken
            .connect(governanceContract)
            .mintTippedLLamas(userA.address, 100_000)
        ).to.not.be.reverted;
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
          100_000
        );
      });
      it("should let the governance contract to mint the max value in a single transaction", async () => {
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        await expect(
          llamaToken
            .connect(governanceContract)
            .mintTippedLLamas(userA.address, tokenCap)
        ).to.not.be.reverted;
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
          tokenCap
        );
      });
      it("should allow the user to fetch the next token ID", async () => {
        expect(await llamaToken.getNextTokenId()).to.equal(4);
        await expect(
          llamaToken
            .connect(governanceContract)
            .newItem([userA.address], [], 100)
        ).not.to.be.reverted;
        expect(await llamaToken.getNextTokenId()).to.equal(5);
      });
      it("should not increment the tokenId counter when minting tippedLlamas", async () => {
        expect(await llamaToken.getNextTokenId()).to.equal(4);
        await expect(
          llamaToken
            .connect(governanceContract)
            .mintTippedLLamas(userA.address, 100)
        ).not.to.be.reverted;
        expect(await llamaToken.getNextTokenId()).to.equal(4);
      });
      it("should let governance contractor mint up to the cap over many transactions to the same user", async () => {
        const capFifth = tokenCap.div(5);
        let currentBalance = BigNumber.from(0);
        for (let txCount = 0; txCount < 5; txCount++) {
          expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
            currentBalance
          );
          await expect(
            llamaToken
              .connect(governanceContract)
              .mintTippedLLamas(userA.address, capFifth)
          ).to.not.be.reverted;
          currentBalance = currentBalance.add(capFifth);
          expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
            currentBalance
          );
        }
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
          tokenCap
        );
      });
      it("should let governance contractor mint up to the cap over many transactions to multiple users", async () => {
        const capFifth = tokenCap.div(5);
        const users = [userA.address, userB.address, userC.address];
        const userBalances = [
          BigNumber.from(0),
          BigNumber.from(0),
          BigNumber.from(0),
        ];
        let totalSupply = BigNumber.from(0);
        for (let txCount = 0; txCount < 5; txCount++) {
          const userIndex = txCount % users.length;
          expect(
            await llamaToken.balanceOf(users[userIndex], tokenId)
          ).to.equal(userBalances[userIndex]);
          await expect(
            llamaToken
              .connect(governanceContract)
              .mintTippedLLamas(users[userIndex], capFifth)
          ).to.not.be.reverted;
          userBalances[userIndex] = userBalances[userIndex].add(capFifth);
          totalSupply = totalSupply.add(capFifth);
          expect(
            await llamaToken.balanceOf(users[userIndex], tokenId)
          ).to.equal(userBalances[userIndex]);
          expect(await llamaToken.totalSupply(tokenId)).to.equal(totalSupply);
        }
        expect(await llamaToken.totalSupply(tokenId)).to.equal(tokenCap);
      });
      it("should not let the governance contractor mint over the token cap", async () => {
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        await expect(
          llamaToken
            .connect(governanceContract)
            .mintTippedLLamas(userA.address, tokenCap.add(1))
        ).to.be.revertedWith("Minting would exceed max supply");
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
      });
      it("should not let the governance contractor mint above the token cap over many transactions to the same user", async () => {
        const capFifth = tokenCap.div(5);
        let currentBalance = BigNumber.from(0);
        for (let txCount = 0; txCount < 5; txCount++) {
          expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
            currentBalance
          );
          await expect(
            llamaToken
              .connect(governanceContract)
              .mintTippedLLamas(userA.address, capFifth)
          ).to.not.be.reverted;
          currentBalance = currentBalance.add(capFifth);
          expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
            currentBalance
          );
        }
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
          tokenCap
        );
        await expect(
          llamaToken
            .connect(governanceContract)
            .mintTippedLLamas(userA.address, 1)
        ).to.be.revertedWith("Minting would exceed max supply");
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(
          tokenCap
        );
      });
      it("should not let the governance contractor mint above the token cap over many transactions to multiple users", async () => {
        const capFifth = tokenCap.div(5);
        const users = [userA.address, userB.address, userC.address];
        const userBalances = [
          BigNumber.from(0),
          BigNumber.from(0),
          BigNumber.from(0),
        ];
        let totalSupply = BigNumber.from(0);
        for (let txCount = 0; txCount < 5; txCount++) {
          const userIndex = txCount % users.length;
          expect(
            await llamaToken.balanceOf(users[userIndex], tokenId)
          ).to.equal(userBalances[userIndex]);
          await expect(
            llamaToken
              .connect(governanceContract)
              .mintTippedLLamas(users[userIndex], capFifth)
          ).to.not.be.reverted;
          userBalances[userIndex] = userBalances[userIndex].add(capFifth);
          totalSupply = totalSupply.add(capFifth);
          expect(
            await llamaToken.balanceOf(users[userIndex], tokenId)
          ).to.equal(userBalances[userIndex]);
          expect(await llamaToken.totalSupply(tokenId)).to.equal(totalSupply);
        }
        expect(await llamaToken.totalSupply(tokenId)).to.equal(tokenCap);
        const userIndex = 2; // UserC balances and address
        expect(await llamaToken.balanceOf(users[userIndex], tokenId)).to.equal(
          userBalances[userIndex]
        );
        await expect(
          llamaToken
            .connect(governanceContract)
            .mintTippedLLamas(users[userIndex], 1)
        ).to.be.revertedWith("Minting would exceed max supply");
        expect(await llamaToken.balanceOf(users[userIndex], tokenId)).to.equal(
          userBalances[userIndex]
        );
        expect(await llamaToken.totalSupply(tokenId)).to.equal(tokenCap);
      });
    });
    describe("Management of URI strings", () => {
      let tippedLlamaURI: string;
      let magicConchURI: string;
      let glitchyLlamaURI: string;
      let oldLlamaURI: string;
      let randomURI: string;

      let tippedLlamaID: BigNumber;
      let magicConchID: BigNumber;
      let glitchyLlamaID: BigNumber;
      let oldLlamaID: BigNumber;
      const randomID = BigNumber.from(256);
      beforeEach(async () => {
        // Set the token ID's
        const tippedIDPromise = llamaToken.TIPPED_LLAMAS();
        const magicIDPromise = llamaToken.MAGIC_CONCH_SHELL();
        const glitchyIDPromise = llamaToken.GLITCHY_LLAMA();
        const oldIDPromise = llamaToken.OLD_LLAMA();
        [tippedLlamaID, magicConchID, glitchyLlamaID, oldLlamaID] =
          await Promise.all([
            tippedIDPromise,
            magicIDPromise,
            glitchyIDPromise,
            oldIDPromise,
          ]);

        const tippedURIPromise = llamaToken.uri(tippedLlamaID);
        const magicURIPromise = llamaToken.uri(magicConchID);
        const glitchyURIPromise = llamaToken.uri(glitchyLlamaID);
        const oldURIPromise = llamaToken.uri(oldLlamaID);
        const randomURIPromise = llamaToken.uri(randomID);

        [
          tippedLlamaURI,
          magicConchURI,
          glitchyLlamaURI,
          oldLlamaURI,
          randomURI,
        ] = await Promise.all([
          tippedURIPromise,
          magicURIPromise,
          glitchyURIPromise,
          oldURIPromise,
          randomURIPromise,
        ]);
      });
      it("should not allow regular users to change the URI", async () => {
        const newURI = "Huzzah, a new URI!";
        const users = [userA, userB, userC, userD, purchaseContract];
        for (const user of users) {
          await expect(
            llamaToken.connect(user).updateURI(newURI)
          ).to.be.revertedWith("Caller is not authorized");
        }
        // Test that none of the URI's have changed
        expect(await llamaToken.uri(tippedLlamaID)).to.equal(tippedLlamaURI);
        expect(await llamaToken.uri(magicConchID)).to.equal(magicConchURI);
        expect(await llamaToken.uri(glitchyLlamaID)).to.equal(glitchyLlamaURI);
        expect(await llamaToken.uri(oldLlamaID)).to.equal(oldLlamaURI);
        expect(await llamaToken.uri(randomID)).to.equal(randomURI);
      });
      it("should allow the governance contract to change the URI", async () => {
        let newURI = "Huzzah, a new URI!";
        await expect(llamaToken.connect(governanceContract).updateURI(newURI))
          .to.not.be.reverted;
        expect(await llamaToken.uri(tippedLlamaID)).to.equal(newURI);
        expect(await llamaToken.uri(magicConchID)).to.equal(newURI);
        expect(await llamaToken.uri(glitchyLlamaID)).to.equal(newURI);
        expect(await llamaToken.uri(oldLlamaID)).to.equal(newURI);
        expect(await llamaToken.uri(randomID)).to.equal(newURI);
      });
    });
    describe("Issuance of new items", () => {
      it("should only allow the governance contract to issue new items", async () => {
        await expect(
          llamaToken.connect(userA).newItem([userA.address], [], 100_000)
        ).to.be.revertedWith("Caller is not minter");
        await expect(
          llamaToken.connect(userB).newItem([userA.address], [], 10_000)
        ).to.be.revertedWith("Caller is not minter");
        await expect(
          llamaToken.connect(userC).newItem([userD.address], [], 1_000)
        ).to.be.revertedWith("Caller is not minter");
        await expect(
          llamaToken
            .connect(userC)
            .newItem(
              [governanceContract.address, purchaseContract.address],
              [],
              0
            )
        ).to.be.revertedWith("Caller is not minter");
        await expect(
          llamaToken.connect(userD).newItem([governanceContract.address], [], 1)
        ).to.be.revertedWith("Caller is not minter");
        await expect(
          llamaToken.connect(userA).newItem([], [], 0)
        ).to.be.revertedWith("Caller is not minter");
        await expect(
          llamaToken
            .connect(purchaseContract)
            .newItem([userA.address], [], 100_000)
        ).to.be.revertedWith("Caller is not minter");
      });
      it("should require there is at least on recipient address", async () => {
        await expect(
          llamaToken.connect(governanceContract).newItem([], [], 0)
        ).to.be.revertedWith("Must have at least one receiving address");
        await expect(
          llamaToken.connect(governanceContract).newItem([], [], 100)
        ).to.be.revertedWith("Must have at least one receiving address");
        await expect(
          llamaToken.connect(governanceContract).newItem([], [0x85], 100)
        ).to.be.revertedWith("Must have at least one receiving address");
      });
      it("should require that at least one item is being minted", async () => {
        await expect(
          llamaToken.connect(governanceContract).newItem([userA.address], [], 0)
        ).to.be.revertedWith("Must have at least quantity of one minted");
      });
      it("should allow the governance contract to mint new items to users", async () => {
        let initialTokenId = BigNumber.from(await getPrivateValue("_tokenIds"));

        expect(
          await llamaToken
            .connect(governanceContract)
            .newItem([userA.address], [], 10)
        )
          .to.emit(llamaToken, "TransferSingle")
          .withArgs(
            governanceContract.address,
            NULL_ADDRESS,
            userA.address,
            initialTokenId.add(1),
            10
          );

        let tokenId = BigNumber.from(await getPrivateValue("_tokenIds"));

        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(10);

        expect(tokenId).to.be.gt(initialTokenId);

        initialTokenId = BigNumber.from(await getPrivateValue("_tokenIds"));

        expect(
          await llamaToken
            .connect(governanceContract)
            .newItem([userC.address], [], 1)
        )
          .to.emit(llamaToken, "TransferSingle")
          .withArgs(
            governanceContract.address,
            NULL_ADDRESS,
            userC.address,
            initialTokenId.add(1),
            1
          );

        tokenId = BigNumber.from(await getPrivateValue("_tokenIds"));

        expect(await llamaToken.balanceOf(userC.address, tokenId)).to.equal(1);
        expect(await llamaToken.balanceOf(userA.address, tokenId)).to.equal(0);
        expect(tokenId).to.be.gt(initialTokenId);
      });
    });
  });
});
