import { expect } from "chai";
import { ethers } from "hardhat";
import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import {
  LlamaToken,
  LlamaToken__factory,
  MockStaker,
  MockStaker__factory,
} from "../typechain-types";
import { BigNumber } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import hh from "hardhat";
import { loadFixture, time } from "@nomicfoundation/hardhat-network-helpers";

// Add duration support to dayjs for timestamp handling
dayjs.extend(duration);

describe("ERC20 Staker", () => {
  let stakerTokenFactory: MockStaker__factory;

  let deployer: SignerWithAddress,
    staker: SignerWithAddress,
    friend: SignerWithAddress,
    userThree: SignerWithAddress,
    userFour: SignerWithAddress;

  const initialMinting = 100_000_000;
  const stakerAmount = 50_000_000;
  const timeLock = dayjs.duration({ months: 1 }).asSeconds();
  const stakeAmount = 250_000;
  const transferAmount = 250_000;

  async function CleanSlateFixture() {}

  before(async () => {
    [deployer, staker, friend, userThree, userFour] = await ethers.getSigners();
    stakerTokenFactory = await ethers.getContractFactory(
      "MockStaker",
      deployer
    );
    await loadFixture(CleanSlateFixture);
  });

  describe("Constructor/Deploying", () => {
    before(async () => {
      await loadFixture(CleanSlateFixture);
    });
    it("should allow a ERC20 contract that inherits ERC20Staker to deploy normally and set values correctly", async () => {
      const token = await stakerTokenFactory.deploy(timeLock, initialMinting);
      expect(await token.decimals()).to.equal(18);
      expect(await token.name()).to.equal("Staker Delights");
      expect(await token.symbol()).to.equal("STDs");
      expect(await token.unbondingPeriod()).to.equal(timeLock);
      expect(await token.balanceOf(deployer.address)).to.equal(initialMinting);
    });
    it("should allow any value for the time lock variable (including zero and max(uint256))", async () => {
      const mintingQuantity = 0;

      let token = await stakerTokenFactory.deploy(0, mintingQuantity);
      expect(await token.unbondingPeriod()).to.equal(0);

      token = await stakerTokenFactory.deploy(1, mintingQuantity);
      expect(await token.unbondingPeriod()).to.equal(1);

      token = await stakerTokenFactory.deploy(100_000, mintingQuantity);
      expect(await token.unbondingPeriod()).to.equal(100_000);

      const reallyLargeNumber = ethers.constants.MaxUint256;

      token = await stakerTokenFactory.deploy(
        reallyLargeNumber,
        mintingQuantity
      );
      expect(await token.unbondingPeriod()).to.equal(reallyLargeNumber);
    });
  });

  describe("Staking", () => {
    let token: MockStaker;

    async function StakingFixture() {
      const token = await stakerTokenFactory.deploy(timeLock, initialMinting);

      const tx = await token.transfer(staker.address, stakerAmount);
      await tx.wait();

      return token;
    }

    before(async () => {
      await loadFixture(CleanSlateFixture);
    });

    beforeEach(async () => {
      token = await loadFixture(StakingFixture);
    });

    it("should allow a user to stake some of the tokens they have and still use their remaining balance as normal", async () => {
      // Stake an initial balance
      await expect(token.connect(staker).stake(stakeAmount))
        .to.emit(token, "TokenStaked")
        .withArgs(staker.address, stakeAmount, stakeAmount);

      // Transfer some of your balance to a friend
      await expect(
        token.connect(staker).transfer(friend.address, transferAmount)
      )
        .to.emit(token, "Transfer")
        .withArgs(staker.address, friend.address, transferAmount);

      const remainingBalance = await token.balanceOf(staker.address);
      const expectedRemainingBalance = stakerAmount - transferAmount;

      expect(remainingBalance).to.equal(expectedRemainingBalance);

      const availableBalance = stakerAmount - (stakeAmount + transferAmount);
      const unavailableBalance = expectedRemainingBalance - availableBalance;

      // Verify balances match what you expect
      expect((await token.staker(staker.address)).unavailableTokens).to.equal(
        unavailableBalance
      );

      // Attempt to transfer all your remaining balance including your staked balance which should revert
      await expect(
        token.connect(staker).transfer(friend.address, remainingBalance)
      )
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(availableBalance, remainingBalance);

      // Attempt to transfer your available balance to a friend
      await expect(
        token.connect(staker).transfer(friend.address, availableBalance)
      )
        .to.emit(token, "Transfer")
        .withArgs(staker.address, friend.address, availableBalance);

      // Verify the only remaining balance is the staked balance
      expect(await token.balanceOf(staker.address)).to.equal(
        unavailableBalance
      );

      // Expect an attempt to transfer even a single extra value to revert
      await expect(token.connect(staker).transfer(friend.address, 1))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(0, 1);

      // If a friend transfers a balance to the user it should be available like normal
      await expect(
        token.connect(friend).transfer(staker.address, transferAmount)
      )
        .to.emit(token, "Transfer")
        .withArgs(friend.address, staker.address, transferAmount);

      // A transfer of one above the newly transferred balance should fail
      await expect(
        token.connect(staker).transfer(friend.address, transferAmount + 1)
      )
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(transferAmount, transferAmount + 1);

      // A transfer of newly transferred balance should succeed
      await expect(
        token.connect(staker).transfer(friend.address, transferAmount)
      )
        .to.emit(token, "Transfer")
        .withArgs(staker.address, friend.address, transferAmount);
    });

    it("should allow calls to internal function _stake to work the same as calls to the external function", async () => {
      // Stake the users entire balance
      expect(
        await token.connect(deployer).__stake(staker.address, stakerAmount)
      )
        .to.emit(token, "TokenStaked")
        .withArgs(staker.address, stakerAmount, stakerAmount);

      // Expect an attempt to transfer even a single extra value to revert
      await expect(token.connect(staker).transfer(friend.address, 1))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(0, 1);

      // If new coins are received from minting they should be free to transfer
      await token.connect(deployer).mint(staker.address, 10);
      await expect(token.connect(staker).transfer(friend.address, 10))
        .to.emit(token, "Transfer")
        .withArgs(staker.address, friend.address, 10);
    });

    it("should revert when the internal function _stake is asked to stake more then a user has", async () => {
      await expect(
        token.connect(deployer).__stake(staker.address, stakerAmount + 1)
      )
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(stakerAmount, stakerAmount + 1);
    });

    it("should revert when calls to the internal function stake zero quantity", async () => {
      await expect(
        token.connect(deployer).__stake(staker.address, 0)
      ).to.be.revertedWithCustomError(token, "ExpectedNonZeroQuantity");
    });

    it("should revert when the internal function _stake is given a NULL/ZERO address", async () => {
      await expect(
        token
          .connect(deployer)
          .__stake(ethers.constants.AddressZero, stakerAmount)
      ).to.be.revertedWithCustomError(token, "InvalidAddress");
    });

    it("should allow a user to stake all of the tokens they have and prevent any transfers", async () => {
      // Stake our entire balance
      await expect(token.connect(staker).stake(stakerAmount))
        .to.emit(token, "TokenStaked")
        .withArgs(staker.address, stakerAmount, stakerAmount);

      // Expect an attempt to transfer even a single extra value to revert
      await expect(token.connect(staker).transfer(friend.address, 1))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(0, 1);

      // If new coins are received from minting they should be free to transfer
      await token.connect(deployer).mint(staker.address, 10);
      await expect(token.connect(staker).transfer(friend.address, 10))
        .to.emit(token, "Transfer")
        .withArgs(staker.address, friend.address, 10);
    });

    it("should revert if multiple calls to stake added together exceed the user balance even if no individual call does", async () => {
      const stakeAmount = stakerAmount / 4;
      let totalStaked = 0;
      // Each call should work correctly as the sum(4 * stakeAmount) === userBalance
      for (let i = 0; i < 4; i++) {
        totalStaked += stakeAmount;
        await expect(token.connect(staker).stake(stakeAmount))
          .to.emit(token, "TokenStaked")
          .withArgs(staker.address, stakeAmount, totalStaked);
      }
      // Even though the next call to stake is still less then the user balance stakeAmount < userBalance
      // it should revert as sum(5 * stakeAmount) >== userBalance
      await expect(token.connect(staker).stake(stakeAmount))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(stakerAmount, stakeAmount);
    });

    it("should allow a user who has staked tokens to stake more tokens they receive at a later time", async () => {
      // Stake our entire balance
      await expect(token.connect(staker).stake(stakerAmount))
        .to.emit(token, "TokenStaked")
        .withArgs(staker.address, stakerAmount, stakerAmount);

      // Get more from a friend
      expect(
        await token.connect(deployer).transfer(staker.address, transferAmount)
      )
        .to.emit(token, "Transfer")
        .withArgs(deployer.address, staker.address, transferAmount);

      // Stake it right away
      const totalStaked = stakerAmount + transferAmount;
      await expect(token.connect(staker).stake(transferAmount))
        .to.emit(token, "TokenStaked")
        .withArgs(staker.address, transferAmount, totalStaked);
      const stakerRecords = await token.staker(staker.address);
      expect(stakerRecords.stakedTokens).to.equal(totalStaked);

      // New transfers should of course fail
      await expect(token.connect(staker).transfer(friend.address, 1))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(0, 1);
    });

    it("should not allow staked balance from a user to be transferred from another user", async () => {
      // Stake our entire balance
      await expect(token.connect(staker).stake(stakerAmount))
        .to.emit(token, "TokenStaked")
        .withArgs(staker.address, stakerAmount, stakerAmount);

      // Transfers from approved others should also fail
      await expect(
        token.connect(staker).approve(friend.address, transferAmount)
      )
        .to.emit(token, "Approval")
        .withArgs(staker.address, friend.address, transferAmount);

      await expect(
        token
          .connect(friend)
          .transferFrom(staker.address, friend.address, transferAmount)
      )
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(0, transferAmount);
    });

    it("should not allow a user to burn their staked funds", async () => {
      // Stake our entire balance
      await expect(token.connect(staker).stake(stakerAmount))
        .to.emit(token, "TokenStaked")
        .withArgs(staker.address, stakerAmount, stakerAmount);

      await expect(token.connect(staker).burn(stakerAmount))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(0, stakerAmount);
    });

    it("should not allow a user to burnFrom their staked funds", async () => {
      // Stake our entire balance
      await expect(token.connect(staker).stake(stakerAmount))
        .to.emit(token, "TokenStaked")
        .withArgs(staker.address, stakerAmount, stakerAmount);

      await expect(token.connect(staker).approve(friend.address, stakerAmount))
        .to.emit(token, "Approval")
        .withArgs(staker.address, friend.address, stakerAmount);

      await expect(token.connect(friend).burnFrom(staker.address, stakerAmount))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(0, stakerAmount);
    });

    it("should revert with ExceededBalance if the amount staked exceeds users funds", async () => {
      await expect(token.connect(staker).stake(stakerAmount + 1))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(stakerAmount, stakerAmount + 1);
    });

    it("should revert if the staking quantity is zero", async () => {
      await expect(
        token.connect(staker).stake(0)
      ).to.be.revertedWithCustomError(token, "ExpectedNonZeroQuantity");
    });
  });

  describe("Slashing", () => {
    let token: MockStaker;
    const slashingAmount = 50_000;

    async function SlashingFixture() {
      const token = await stakerTokenFactory.deploy(timeLock, initialMinting);
      await token.transfer(staker.address, stakerAmount);
      const tx = await token.connect(staker).stake(stakeAmount);
      await tx.wait();
      return token;
    }

    before(async () => {
      await loadFixture(CleanSlateFixture);
    });

    beforeEach(async () => {
      token = await loadFixture(SlashingFixture);
    });

    it("should allow us to slash a user who has staked assets", async () => {
      const remainingStakedAmount = stakeAmount - slashingAmount;
      const remainingBalance = stakerAmount - slashingAmount;
      const unbondingAmount = 0;
      await expect(token.slash(staker.address, slashingAmount))
        .to.emit(token, "TokenSlashed")
        .withArgs(
          staker.address,
          deployer.address,
          slashingAmount,
          remainingStakedAmount,
          unbondingAmount
        );
      expect(await token.balanceOf(staker.address)).to.equal(remainingBalance);
      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        remainingStakedAmount
      );
    });

    it("should allow us to slash a user of all their staked assets", async () => {
      const remainingStakedAmount = 0;
      const unbondingAmount = 0;
      const remainingBalance = stakerAmount - stakeAmount;
      await expect(token.slash(staker.address, stakeAmount))
        .to.emit(token, "TokenSlashed")
        .withArgs(
          staker.address,
          deployer.address,
          stakeAmount,
          remainingStakedAmount,
          unbondingAmount
        );
      expect(await token.balanceOf(staker.address)).to.equal(remainingBalance);
      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        remainingStakedAmount
      );
    });
    it("should allow us to slash an entire users balance if they staked their entire balance", async () => {
      const remaining = 0;
      // First we must stake all their tokens
      await token.connect(staker).stake(stakerAmount - stakeAmount); // User already has somed staked tokens, just stake the rest
      await expect(token.slash(staker.address, stakerAmount))
        .to.emit(token, "TokenSlashed")
        .withArgs(
          staker.address,
          deployer.address,
          stakerAmount,
          remaining,
          remaining
        );
      expect(await token.balanceOf(staker.address)).to.equal(remaining);
      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        remaining
      );
    });
    it("should allow us to slash a user who is unbonding some staked assets", async () => {
      const unbondAmount = stakeAmount / 2;
      await token.connect(staker).unbond(unbondAmount);
      // Slash 3/4th of the staked and unbonding funds
      const slashAmount = stakeAmount - stakeAmount / 4;
      const expectedRemainingStakedTokens = 0;
      // The slashed amount should be taken from the staked tokens before the unbonding tokens
      const expectedRemainingUnbondingTokens = stakeAmount / 4;
      await expect(token.slash(staker.address, slashAmount))
        .to.emit(token, "TokenSlashed")
        .withArgs(
          staker.address,
          deployer.address,
          slashAmount,
          expectedRemainingStakedTokens,
          expectedRemainingUnbondingTokens
        );
      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        expectedRemainingStakedTokens
      );
      const expectedBalance = stakerAmount - slashAmount;
      expect(await token.balanceOf(staker.address)).to.equal(expectedBalance);
    });
    it("should allow us to slash a user who is unbonding all their staked assets", async () => {
      const unbondAmount = stakeAmount;
      await token.connect(staker).unbond(unbondAmount);
      // Slash 3/4th of the staked and unbonding funds
      const slashAmount = stakeAmount - stakeAmount / 4;
      const expectedRemainingStakedTokens = 0;
      // The slashed amount should be taken from the staked tokens before the unbonding tokens
      const expectedRemainingUnbondingTokens = stakeAmount / 4;
      await expect(token.slash(staker.address, slashAmount))
        .to.emit(token, "TokenSlashed")
        .withArgs(
          staker.address,
          deployer.address,
          slashAmount,
          expectedRemainingStakedTokens,
          expectedRemainingUnbondingTokens
        );
      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        expectedRemainingStakedTokens
      );
      const expectedBalance = stakerAmount - slashAmount;
      expect(await token.balanceOf(staker.address)).to.equal(expectedBalance);
    });
    it("should allow us to slash all the unbonding tokens of a user who is unbonding all of staked tokens", async () => {
      const unbondAmount = stakeAmount;
      const remaningAfterSlash = 0;
      await token.connect(staker).unbond(unbondAmount);
      await expect(token.slash(staker.address, stakeAmount))
        .to.emit(token, "TokenSlashed")
        .withArgs(
          staker.address,
          deployer.address,
          stakeAmount,
          remaningAfterSlash,
          remaningAfterSlash
        );
      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        remaningAfterSlash
      );
      expect((await token.staker(staker.address)).unbondingTokens).to.equal(
        remaningAfterSlash
      );
      expect(await token.balanceOf(staker.address)).to.equal(
        stakerAmount - stakeAmount
      );
    });
    it("should not allow us to slash someone who has unstaked", async () => {
      // Unbond Request
      await token.connect(staker).unbond(stakeAmount);
      // Wait time-out period
      await time.increase(timeLock);
      await token.connect(staker).unstake();

      const remainingStake = 0;

      // Expect a slash amount for any amount to revert
      const slashRequest = 1;

      await expect(token.slash(staker.address, 1))
        .to.revertedWithCustomError(token, "ExcessiveSlash")
        .withArgs(remainingStake, slashRequest);

      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        remainingStake
      );
      expect((await token.staker(staker.address)).unavailableTokens).to.equal(
        remainingStake
      );
      // Expect the full staker balance to be remaining
      expect(await token.balanceOf(staker.address)).to.equal(stakerAmount);
    });
    it("should not allow us to slash in excess of a users total staked and unbonding balance", async () => {
      const unbondAmount = stakeAmount / 2;
      await token.connect(staker).unbond(unbondAmount);
      // Slash 3/4th of the staked and unbonding funds
      const slashAmount = stakeAmount * 2;
      const expectedRemainingStakedTokens = stakeAmount - unbondAmount;
      const expectedRemainingUnavailableTokens = stakeAmount;
      // The slashed amount should be taken from the staked tokens before the unbonding tokens
      const expectedRemainingUnbondingTokens = unbondAmount;

      await expect(token.slash(staker.address, slashAmount))
        .to.be.revertedWithCustomError(token, "ExcessiveSlash")
        .withArgs(stakeAmount, slashAmount);

      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        expectedRemainingStakedTokens
      );
      expect((await token.staker(staker.address)).unavailableTokens).to.equal(
        expectedRemainingUnavailableTokens
      );
      // Expect the full staker balance to be remaining
      expect(await token.balanceOf(staker.address)).to.equal(stakerAmount);
    });
    it("should not allow us to slash a non-staked user", async () => {
      // Give userThree account money to test slashing on
      await token.transfer(userThree.address, stakerAmount);

      const unavailableTokens = 0;
      const beforeSlashStakerRecord = await token.staker(userThree.address);
      const NoUnbondingRequest = 0;
      // No amount of slashing should be permitted
      const slashAmount = 1;

      // Test assertion
      expect(stakerAmount).greaterThan(slashAmount);

      expect(beforeSlashStakerRecord.stakedTokens).to.equal(unavailableTokens);
      expect(beforeSlashStakerRecord.unbondingTokens).to.equal(
        unavailableTokens
      );
      expect(beforeSlashStakerRecord.unbondingLockOutTimeStamp).to.equal(
        NoUnbondingRequest
      );

      await expect(token.slash(userThree.address, slashAmount))
        .to.be.revertedWithCustomError(token, "ExcessiveSlash")
        .withArgs(unavailableTokens, slashAmount);

      const afterSlashStakerRecord = await token.staker(userThree.address);
      expect(afterSlashStakerRecord.stakedTokens).to.equal(unavailableTokens);
      expect(afterSlashStakerRecord.unbondingTokens).to.equal(
        unavailableTokens
      );
      expect(afterSlashStakerRecord.unbondingLockOutTimeStamp).to.equal(
        NoUnbondingRequest
      );
    });
    it("should not allow us to slash a user with zero balance", async () => {
      const unavailableTokens = 0;
      const beforeSlashStakerRecord = await token.staker(userThree.address);
      const NoUnbondingRequest = 0;
      // No amount of slashing should be permitted
      const slashAmount = 1;

      // Test assertion
      expect(stakerAmount).greaterThan(slashAmount);

      expect(await token.balanceOf(userThree.address)).to.equal(0);
      expect(beforeSlashStakerRecord.stakedTokens).to.equal(unavailableTokens);
      expect(beforeSlashStakerRecord.unbondingTokens).to.equal(
        unavailableTokens
      );
      expect(beforeSlashStakerRecord.unbondingLockOutTimeStamp).to.equal(
        NoUnbondingRequest
      );

      await expect(token.slash(userThree.address, slashAmount))
        .to.be.revertedWithCustomError(token, "ExcessiveSlash")
        .withArgs(unavailableTokens, slashAmount);

      const afterSlashStakerRecord = await token.staker(userThree.address);
      expect(afterSlashStakerRecord.stakedTokens).to.equal(unavailableTokens);
      expect(afterSlashStakerRecord.unbondingTokens).to.equal(
        unavailableTokens
      );
      expect(afterSlashStakerRecord.unbondingLockOutTimeStamp).to.equal(
        NoUnbondingRequest
      );
    });
    it("should revert if given the ZERO/NULL address for slashing", async () => {
      await expect(
        token.slash(ethers.constants.AddressZero, 1)
      ).to.be.revertedWithCustomError(token, "InvalidAddress");
    });
    it("should revert if the slashing quantity is zero", async () => {
      const slashingAmount = 0;
      await expect(
        token.slash(staker.address, slashingAmount)
      ).to.be.revertedWithCustomError(token, "ExpectedNonZeroQuantity");
      expect(await token.balanceOf(staker.address)).to.equal(stakerAmount);
      expect((await token.staker(staker.address)).stakedTokens).to.equal(
        stakeAmount
      );
    });
  });

  describe("Unbonding", () => {
    let token: MockStaker;
    async function UnbondingFixture() {
      const token = await stakerTokenFactory.deploy(timeLock, initialMinting);
      await token.transfer(staker.address, stakerAmount);
      const tx = await token.connect(staker).stake(stakeAmount);
      await tx.wait();
      return token;
    }

    before(async () => {
      await loadFixture(CleanSlateFixture);
    });

    beforeEach(async () => {
      token = await loadFixture(UnbondingFixture);
    });

    it("should allow a user to unbond any quantity of staked tokens", async () => {
      const unbondAmounts = [1, stakeAmount / 4, stakeAmount / 2, stakeAmount];
      for (const [index, unbondAmount] of unbondAmounts.entries()) {
        const timeStamp = dayjs().unix() + 60 + index; // Make sure we are always in the future between calls of setNextTimestamp()
        const timeLockOut = timeStamp + timeLock;
        const stakedTokens = stakeAmount - unbondAmount;
        await time.setNextBlockTimestamp(timeStamp);

        await expect(token.connect(staker).unbond(unbondAmount))
          .to.emit(token, "TokenUnbonding")
          .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
        const StakerRecord = await token.staker(staker.address);
        expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
        expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
        expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
      }
    });
    it("should allow the internal function to submit an unbond request for staked tokens", async () => {
      const unbondAmounts = [1, stakeAmount / 4, stakeAmount / 2, stakeAmount];
      for (const [index, unbondAmount] of unbondAmounts.entries()) {
        const timeStamp = dayjs().unix() + 60 + index; // Make sure we are always in the future between calls of setNextTimestamp()
        const timeLockOut = timeStamp + timeLock;
        const stakedTokens = stakeAmount - unbondAmount;
        await time.setNextBlockTimestamp(timeStamp);

        await expect(token.__unbond(staker.address, unbondAmount))
          .to.emit(token, "TokenUnbonding")
          .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
        const StakerRecord = await token.staker(staker.address);
        expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
        expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
        expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
      }
    });
    it("should revert if the unbond request of zero is submitted to the internal function", async () => {
      await expect(
        token.__unbond(staker.address, 0)
      ).to.be.revertedWithCustomError(token, "ExpectedNonZeroQuantity");
    });
    it("should reset the unbond request if the external unbond function is given a quantity of zero", async () => {
      await token.connect(staker).unbond(stakeAmount);
      const unbondAmount = 0;
      const timeLockOut = 0;
      const stakedTokens = stakeAmount - unbondAmount;

      await expect(token.connect(staker).unbond(unbondAmount))
        .to.emit(token, "TokenUnbonding")
        .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
      const StakerRecord = await token.staker(staker.address);
      expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
      expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
      expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
    });
    it("should revert if the internal _cancelUnbond is given a zero address", async () => {
      await expect(
        token.__cancelUnbond(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(token, "InvalidAddress");
    });
    it("should reset an unbond request if the internal _cancelUnbond is given a valid address", async () => {
      await token.connect(staker).unbond(stakeAmount);
      const unbondAmount = 0;
      const timeLockOut = 0;
      const stakedTokens = stakeAmount - unbondAmount;

      await expect(token.__cancelUnbond(staker.address))
        .to.emit(token, "TokenUnbonding")
        .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
      const StakerRecord = await token.staker(staker.address);
      expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
      expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
      expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
    });
    it("should allow the internal function to reset an unbond request by overwriting the old one", async () => {
      await token.connect(staker).unbond(stakeAmount);
      const timeStamp = dayjs().unix() + 60; // Make sure we are always in the future between calls of setNextTimestamp()
      const timeLockOut = timeStamp + timeLock;
      const unbondAmount = stakeAmount / 2;
      const stakedTokens = stakeAmount - unbondAmount;
      await time.setNextBlockTimestamp(timeStamp);

      await expect(token.__unbond(staker.address, unbondAmount))
        .to.emit(token, "TokenUnbonding")
        .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
      const StakerRecord = await token.staker(staker.address);
      expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
      expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
      expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
    });
    it("should revert if the ZERO/NULL address is given for unbonding", async () => {
      await expect(
        token.__unbond(ethers.constants.AddressZero, 1)
      ).to.be.revertedWithCustomError(token, "InvalidAddress");
    });
    it("should revert if the unbond request exceeds a users total staked/unbonding balance", async () => {
      const unbondAmount = stakeAmount + 1;
      const timeLockOut = 0;
      await expect(token.connect(staker).unbond(unbondAmount))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(stakeAmount, unbondAmount);
      const StakerRecord = await token.staker(staker.address);
      expect(StakerRecord.stakedTokens).to.equal(stakeAmount);
      expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
      expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
    });
    it("should revert if an internal _unbond request exceeds a users total staked/unbonding balance", async () => {
      const unbondAmount = stakeAmount + 1;
      const timeLockOut = 0;
      await expect(token.__unbond(staker.address, unbondAmount))
        .to.be.revertedWithCustomError(token, "ExceededBalance")
        .withArgs(stakeAmount, unbondAmount);
      const StakerRecord = await token.staker(staker.address);
      expect(StakerRecord.stakedTokens).to.equal(stakeAmount);
      expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
      expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
    });
    it("An internal unbond request should override an external unbond request", async () => {
      // External Request
      {
        const unbondAmount = stakeAmount;
        const timeStamp = dayjs().unix() + 60; // Make sure we are always in the future between calls of setNextTimestamp()
        const timeLockOut = timeStamp + timeLock;
        const stakedTokens = stakeAmount - unbondAmount;
        await time.setNextBlockTimestamp(timeStamp);

        await expect(token.connect(staker).unbond(unbondAmount))
          .to.emit(token, "TokenUnbonding")
          .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
        const StakerRecord = await token.staker(staker.address);
        expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
        expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
        expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
      }
      // Internal Request
      {
        const unbondAmount = stakeAmount / 2;
        const timeStamp = dayjs().unix() + 61; // Make sure we are always in the future between calls of setNextTimestamp()
        const timeLockOut = timeStamp + timeLock;
        const stakedTokens = stakeAmount - unbondAmount;
        await time.setNextBlockTimestamp(timeStamp);

        await expect(token.__unbond(staker.address, unbondAmount))
          .to.emit(token, "TokenUnbonding")
          .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
        const StakerRecord = await token.staker(staker.address);
        expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
        expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
        expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
      }
    });
    it("An external unbond request should override an internal unbond request", async () => {
      // Internal Request
      {
        const unbondAmount = stakeAmount / 2;
        const timeStamp = dayjs().unix() + 60; // Make sure we are always in the future between calls of setNextTimestamp()
        const timeLockOut = timeStamp + timeLock;
        const stakedTokens = stakeAmount - unbondAmount;
        await time.setNextBlockTimestamp(timeStamp);

        await expect(token.__unbond(staker.address, unbondAmount))
          .to.emit(token, "TokenUnbonding")
          .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
        const StakerRecord = await token.staker(staker.address);
        expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
        expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
        expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
      }
      // External Request
      {
        const unbondAmount = stakeAmount;
        const timeStamp = dayjs().unix() + 61; // Make sure we are always in the future between calls of setNextTimestamp()
        const timeLockOut = timeStamp + timeLock;
        const stakedTokens = stakeAmount - unbondAmount;
        await time.setNextBlockTimestamp(timeStamp);

        await expect(token.connect(staker).unbond(unbondAmount))
          .to.emit(token, "TokenUnbonding")
          .withArgs(staker.address, unbondAmount, stakedTokens, timeLockOut);
        const StakerRecord = await token.staker(staker.address);
        expect(StakerRecord.stakedTokens).to.equal(stakedTokens);
        expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(timeLockOut);
        expect(StakerRecord.unavailableTokens).to.equal(stakeAmount);
      }
    });
  });

  describe("Unstaking", () => {
    let token: MockStaker;
    const unbondAmount = stakeAmount / 2;
    const timeOffset = 100; // Set ourselves 60 seconds in the future
    const startingTime = dayjs().unix() + timeOffset;
    // Staker has staked an amount and put in an unbond request for half out
    // userThree has staked an amount and put in no unbond request
    async function UnstakingFixture() {
      const token = await stakerTokenFactory.deploy(timeLock, initialMinting);
      await token.transfer(staker.address, stakerAmount);
      await token.transfer(userThree.address, stakerAmount);
      await token.connect(staker).stake(stakeAmount);
      await token.connect(userThree).stake(stakeAmount);
      await time.setNextBlockTimestamp(startingTime);
      const tx = await token.connect(staker).unbond(unbondAmount);
      await tx.wait();
      return token;
    }

    before(async () => {
      await loadFixture(CleanSlateFixture);
    });

    beforeEach(async () => {
      token = await loadFixture(UnstakingFixture);
    });

    it("should allow a user who has an unbonding request and has waited longer then the timeout to unstake", async () => {
      const setTime = startingTime + dayjs.duration({ months: 2 }).asSeconds();
      await time.setNextBlockTimestamp(setTime);
      const remainingStake = stakeAmount - unbondAmount;
      await expect(token.connect(staker).unstake())
        .to.emit(token, "TokenUnstaked")
        .withArgs(staker.address, unbondAmount, remainingStake);
      const StakerRecord = await token.staker(staker.address);
      expect(StakerRecord.stakedTokens).to.equal(remainingStake);
      expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(0); // Should reset to 0
      expect(StakerRecord.unavailableTokens).to.equal(remainingStake);
    });
    it("should allow the internal function to unstake a user who has an unbonding request and has waited longer then the timeout to unstake", async () => {
      const setTime = startingTime + dayjs.duration({ months: 2 }).asSeconds();
      await time.setNextBlockTimestamp(setTime);
      const remainingStake = stakeAmount - unbondAmount;
      await expect(token.__unstake(staker.address))
        .to.emit(token, "TokenUnstaked")
        .withArgs(staker.address, unbondAmount, remainingStake);
      const StakerRecord = await token.staker(staker.address);
      expect(StakerRecord.stakedTokens).to.equal(remainingStake);
      expect(StakerRecord.unbondingLockOutTimeStamp).to.equal(0); // Should reset to 0
      expect(StakerRecord.unavailableTokens).to.equal(remainingStake);
    });
    it("should revert if there was no unbonding request", async () => {
      await expect(
        token.connect(userThree).unstake()
      ).to.be.revertedWithCustomError(token, "NoUnbondingRequest");
    });
    it("should revert if the unbonding request was canceled before the time lockout", async () => {
      await token.connect(userThree).unbond(stakeAmount);

      await token.connect(staker).unbond(0);
      await token.__cancelUnbond(userThree.address);

      const setTime = startingTime + dayjs.duration({ months: 2 }).asSeconds();
      await time.setNextBlockTimestamp(setTime);

      await expect(
        token.connect(staker).unstake()
      ).to.be.revertedWithCustomError(token, "NoUnbondingRequest");

      await expect(
        token.connect(userThree).unstake()
      ).to.be.revertedWithCustomError(token, "NoUnbondingRequest");
    });
    it("should revert if the unbonding request was canceled after the time lockout", async () => {
      await token.connect(userThree).unbond(stakeAmount);

      const setTime = startingTime + dayjs.duration({ months: 2 }).asSeconds();
      await time.setNextBlockTimestamp(setTime);

      await token.connect(staker).unbond(0);
      await token.__cancelUnbond(userThree.address);

      await expect(
        token.connect(staker).unstake()
      ).to.be.revertedWithCustomError(token, "NoUnbondingRequest");

      await expect(
        token.connect(userThree).unstake()
      ).to.be.revertedWithCustomError(token, "NoUnbondingRequest");
    });
    it("should revert if the unbonding request has not completed the time-locked period for unbonding", async () => {
      const TimeLockOut = startingTime + timeLock;
      const timeAttepmts = [
        startingTime + 1,
        startingTime + Math.floor(timeLock / 4),
        startingTime + Math.floor(timeLock / 2),
        TimeLockOut - 1,
      ];
      for (const timeAttempt of timeAttepmts) {
        await time.setNextBlockTimestamp(timeAttempt);
        await expect(token.connect(staker).unstake())
          .to.be.revertedWithCustomError(token, "TimeLockOut")
          .withArgs(timeAttempt, TimeLockOut);
      }
    });
    it("should revert if the internal function to unstake is given a null address", async () => {
      await expect(
        token.__unstake(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(token, "InvalidAddress");
    });
    it("should revert if the internal function to unstake is given an account that has not given an unbonding request", async () => {
      await expect(
        token.__unstake(userThree.address)
      ).to.be.revertedWithCustomError(token, "NoUnbondingRequest");
    });
    it("should revert if the internal function to unstake was given an account that has not waited the full unbonding period", async () => {
      const waitTime = Math.floor(startingTime + timeLock / 2);
      const lockoutTime = startingTime + timeLock;
      await time.setNextBlockTimestamp(waitTime);

      await expect(token.__unstake(staker.address))
        .to.be.revertedWithCustomError(token, "TimeLockOut")
        .withArgs(waitTime, lockoutTime);
    });
    it("should make sure unbond requests after a claimed unstake cannot bypass the timelock period", async () => {
      const lockoutTime = startingTime + timeLock;
      await time.setNextBlockTimestamp(lockoutTime);
      await token.connect(staker).unstake();

      // Try to unbond the rest
      const tx = await token.connect(staker).unbond(unbondAmount);
      // New expected lockoutTime
      const timeStamp = await time.latest();
      const nextBlockTimeStamp = timeStamp + 1;
      await time.setNextBlockTimestamp(nextBlockTimeStamp);
      const newLockoutTime = timeStamp + timeLock;
      // Try to unstake right after
      await expect(token.connect(staker).unstake())
        .to.be.revertedWithCustomError(token, "TimeLockOut")
        .withArgs(nextBlockTimeStamp, newLockoutTime);
    });
  });

  describe("Hooks", () => {
    let token: MockStaker;
    async function HooksFixture() {
      const token = await stakerTokenFactory.deploy(timeLock, initialMinting);

      const tx = await token.transfer(staker.address, stakerAmount);
      await tx.wait();

      return token;
    }

    before(async () => {
      await loadFixture(CleanSlateFixture);
    });

    beforeEach(async () => {
      token = await loadFixture(HooksFixture);
    });

    it("should properly call the before and after hooks for token staking in the correct order", async () => {
      await expect(token.connect(staker).stake(stakeAmount))
        .to.emit(token, "BeforeTokenStake")
        .withArgs(staker.address, stakeAmount, [0, 0, 0, 0])
        .and.to.emit(token, "AfterTokenStake")
        .withArgs(staker.address, stakeAmount, [
          stakeAmount,
          0,
          0,
          stakeAmount,
        ]);
    });
    it("should properly call the before and after hooks for token slashing in the correct order", async () => {
      const slashAmount = Math.floor(stakeAmount / 2);
      const remainingStake = stakeAmount - slashAmount;
      // Stake so we can slash
      await token.connect(staker).stake(stakeAmount);
      await expect(token.slash(staker.address, slashAmount))
        .to.emit(token, "BeforeTokenSlash")
        .withArgs(staker.address, slashAmount, [stakeAmount, 0, 0, stakeAmount])
        .and.to.emit(token, "AfterTokenSlash")
        .withArgs(staker.address, slashAmount, [
          remainingStake,
          0,
          0,
          remainingStake,
        ]);
    });
    it("should properly call the before and after hooks for token unbonding in the correct order", async () => {
      const unbondAmount = Math.floor(stakeAmount / 2);
      const remainingStake = stakeAmount - unbondAmount;
      const timeStamp = dayjs().unix() + 60;
      // Stake so we can unbond
      await token.connect(staker).stake(stakeAmount);
      await time.setNextBlockTimestamp(timeStamp);
      await expect(token.connect(staker).unbond(unbondAmount))
        .to.emit(token, "BeforeTokenUnbonding")
        .withArgs(staker.address, unbondAmount, [
          stakeAmount,
          0,
          0,
          stakeAmount,
        ])
        .and.to.emit(token, "AfterTokenUnbonding")
        .withArgs(staker.address, unbondAmount, [
          remainingStake,
          unbondAmount,
          timeStamp + timeLock,
          stakeAmount,
        ]);
    });
    it("should properly call the before and after hooks for token unstaking in the correct order", async () => {
      const unbondAmount = Math.floor(stakeAmount / 2);
      const remainingStake = stakeAmount - unbondAmount;
      // Stake so we can unbond
      await token.connect(staker).stake(stakeAmount);
      // Unbond so we can unstake
      const timeStamp = dayjs().unix() + 60;
      const timeUnlock = timeStamp + timeLock;
      time.setNextBlockTimestamp(timeStamp);
      await token.connect(staker).unbond(unbondAmount);
      // Wait so we can unstake
      const timeWait = dayjs.duration({ months: 2 }).asSeconds();
      time.increase(timeWait);
      await expect(token.connect(staker).unstake())
        .to.emit(token, "BeforeTokenUnstake")
        .withArgs(staker.address, [
          remainingStake,
          unbondAmount,
          timeUnlock,
          stakeAmount,
        ])
        .and.to.emit(token, "AfterTokenUnstake")
        .withArgs(staker.address, [remainingStake, 0, 0, remainingStake]);
    });
  });
});
