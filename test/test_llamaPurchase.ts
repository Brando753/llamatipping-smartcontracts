import { expect } from "chai";
import { ethers } from "hardhat";
import { BigNumber, Event } from "ethers";
import {
  LlamaDirectory__factory,
  LlamaGovernor__factory,
  LlamaPurchase,
  LlamaPurchase__factory,
  LlamaShares,
  LlamaToken,
  LlamaToken__factory,
  MockToken,
} from "../typechain-types";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Sign } from "crypto";
import { getContractAddress } from "@ethersproject/address";
import { assert } from "console";
import { Result } from "ethers/lib/utils";
import { getEventListeners } from "events";
import { parse } from "path";
import exp from "constants";

const parseEther = ethers.utils.parseEther;

interface BatchTokens {
  purchasePrice: number;
  probability: number;
  quantity: number;
  id: BigNumber;
}

describe("Llama Purchase Contracts", () => {
  let llamaPurchaseFactory: LlamaPurchase__factory;
  let llamaPurchase: LlamaPurchase;
  let llamaToken: LlamaToken;
  let llamaShares: LlamaShares;
  const overrideHeight = 300;
  const blockResetHeight = 900;
  let UserA: SignerWithAddress,
    UserB: SignerWithAddress,
    UserC: SignerWithAddress,
    UserD: SignerWithAddress,
    GovernanceContract: SignerWithAddress;
  let EntropyProviderA: SignerWithAddress, EntropyProviderB: SignerWithAddress;
  let purchaseContractAddress: string;
  let tokenContractAddress: string;
  beforeEach(async () => {
    // Some math needs us to be many blocks in the future
    await ethers.provider.send("hardhat_mine", ["0x5DC"]);
    [
      UserA,
      UserB,
      UserC,
      UserD,
      EntropyProviderA,
      EntropyProviderB,
      GovernanceContract,
    ] = await ethers.getSigners();
    const llamaSharesFactory = await ethers.getContractFactory("LlamaShares");
    llamaShares = await llamaSharesFactory.deploy(
      [UserA.address, UserB.address, UserC.address],
      [parseEther("50"), parseEther("25"), parseEther("25")]
    );
    llamaPurchaseFactory = await ethers.getContractFactory("LlamaPurchase");
    const deployer = llamaPurchaseFactory.signer;
    tokenContractAddress = getContractAddress({
      from: await deployer.getAddress(),
      nonce: await deployer.getTransactionCount(),
    });
    purchaseContractAddress = getContractAddress({
      from: await deployer.getAddress(),
      nonce: (await deployer.getTransactionCount()) + 1,
    });
    const llamaTokenFactory = await ethers.getContractFactory("LlamaToken");
    llamaToken = await llamaTokenFactory.deploy(
      [UserA.address, UserB.address, UserC.address],
      [UserA.address, UserB.address],
      [100_000, 100_000, 100_000],
      UserA.address,
      GovernanceContract.address,
      purchaseContractAddress
    );
    expect(llamaToken.address).to.equal(
      tokenContractAddress,
      "Pre-calculated Llama Token contract address and deploy address do not match!"
    );
    llamaPurchase = await llamaPurchaseFactory.deploy(
      [EntropyProviderA.address, EntropyProviderB.address],
      GovernanceContract.address,
      tokenContractAddress,
      llamaShares.address,
      overrideHeight,
      blockResetHeight,
      parseEther("1") // minimumDonation
    );
    expect(llamaPurchase.address).to.equal(
      purchaseContractAddress,
      "Pre-calculated Llama Purchase contract address and deploy address do not match!"
    );
  });
  describe("Idiot Operations", () => {
    let MockToken: MockToken;
    beforeEach(async () => {
      const MockTokenFactory = await ethers.getContractFactory("MockToken");
      MockToken = await MockTokenFactory.deploy();
      await MockToken.mintBatch(
        UserA.address,
        [0, 1, 2],
        [100_000, 100_000, 100_000],
        []
      );
    });
    it("should not allow an ERC1155 transfer of tokens other then the LLamaToken", async () => {
      await expect(
        MockToken.safeTransferFrom(
          UserA.address,
          llamaPurchase.address,
          0,
          100,
          []
        )
      ).to.be.revertedWith("Only can accept llama tokens");
    });
    it("should not allow a batch transfer of ERC1155 tokens other then the LlamaToken", async () => {
      await expect(
        MockToken.safeBatchTransferFrom(
          UserA.address,
          llamaPurchase.address,
          [0, 1, 2],
          [100, 100, 100],
          []
        )
      ).to.be.revertedWith("Only can accept llama tokens");
    });
  });
  describe("Governance Operations", () => {
    let batchTokens: BatchTokens[];
    beforeEach(async () => {
      const purchasePrice = 1000;
      const probability = 500;
      const id1 = await llamaToken.getNextTokenId();
      const id2 = id1.add(1);
      const id3 = id2.add(1);
      batchTokens = [
        {
          purchasePrice,
          probability,
          quantity: 50,
          id: id1,
        },
        {
          purchasePrice,
          probability,
          quantity: 500,
          id: id2,
        },
        {
          purchasePrice,
          probability,
          quantity: 10,
          id: id3,
        },
      ];
    });
    it("should allow the governance contract to setup a token and send the token to the contract", async () => {
      const id = await llamaToken.getNextTokenId();
      const quantity = 50;
      await expect(
        llamaPurchase.connect(GovernanceContract).addToken(id, 1000, 500)
      ).to.not.be.reverted;
      await expect(
        llamaToken
          .connect(GovernanceContract)
          .newItem([llamaPurchase.address], [], quantity)
      )
        .to.emit(llamaPurchase, "TokensAdded")
        .withArgs(id, quantity);
    });
    it("should allow the governance contract to setup multiple tokens and then batch send them to the contract", async () => {
      for (const token of batchTokens) {
        await expect(
          llamaPurchase
            .connect(GovernanceContract)
            .addToken(token.id, token.purchasePrice, token.probability)
        ).to.not.be.reverted;
        await expect(
          llamaToken
            .connect(GovernanceContract)
            .newItem([GovernanceContract.address], [], token.quantity)
        ).to.not.be.reverted;
      }
      const tokenIds = batchTokens.map((token) => token.id);
      const tokenQuantity = batchTokens.map((token) => token.quantity);
      await expect(
        llamaToken
          .connect(GovernanceContract)
          .safeBatchTransferFrom(
            GovernanceContract.address,
            llamaPurchase.address,
            tokenIds,
            tokenQuantity,
            []
          )
      )
        .to.emit(llamaPurchase, "TokensAdded")
        .withArgs(tokenIds[0], tokenQuantity[0])
        .and.to.emit(llamaPurchase, "TokensAdded")
        .withArgs(tokenIds[1], tokenQuantity[1])
        .and.to.emit(llamaPurchase, "TokensAdded")
        .withArgs(tokenIds[2], tokenQuantity[2]);
    });
    it("should not let the governance contract send a token that is not setup", async () => {
      const quantity = 1000;
      await expect(
        llamaToken
          .connect(GovernanceContract)
          .newItem([llamaPurchase.address], [], quantity)
      ).to.be.revertedWith("Token is not setup");
    });
    it("should not let the governance contract batch send a token that is not setup", async () => {
      const tokenId = [];
      const tokenQuantity = [10, 1, 500, 325];
      for (const quantity of tokenQuantity) {
        const mintTx = await llamaToken
          .connect(GovernanceContract)
          .newItem([GovernanceContract.address], [], quantity);
        const rc = await mintTx.wait();
        const mintEvent = (rc.events as unknown as Event[]).find(
          (event) => event.event === "TransferSingle"
        );
        expect(mintEvent).not.to.be.undefined;
        const definedEvent = mintEvent as Event;
        expect(definedEvent.args).not.to.be.undefined;
        const eventArgs = definedEvent.args as Result;
        tokenId.push(eventArgs[3]);
      }
      await expect(
        llamaToken
          .connect(GovernanceContract)
          .safeBatchTransferFrom(
            GovernanceContract.address,
            llamaPurchase.address,
            tokenId,
            tokenQuantity,
            []
          )
      ).to.be.revertedWith("Some tokens are not setup");
    });
    it("should allow the governance contract to delete a setup token that has a supply and burn that remaining supply", async () => {
      const id = await llamaToken.getNextTokenId();
      const quantity = 50;
      await expect(
        llamaPurchase.connect(GovernanceContract).addToken(id, 1000, 500)
      ).to.not.be.reverted;
      await expect(
        llamaToken
          .connect(GovernanceContract)
          .newItem([llamaPurchase.address], [], quantity)
      )
        .to.emit(llamaPurchase, "TokensAdded")
        .withArgs(id, quantity);

      await expect(llamaPurchase.connect(GovernanceContract).deleteToken(id))
        .to.emit(llamaPurchase, "TokensDeleted")
        .withArgs(id, quantity);
    });
    it("should allow the governance contract to delete a setup token that has no supply", async () => {
      const id = await llamaToken.getNextTokenId();
      await expect(
        llamaPurchase.connect(GovernanceContract).addToken(id, 1000, 500)
      ).to.not.be.reverted;
      await expect(llamaPurchase.connect(GovernanceContract).deleteToken(id))
        .to.emit(llamaPurchase, "TokensDeleted")
        .withArgs(id, 0);
    });
    it("should allow the governance contract to delete a non-setup token without error", async () => {
      const id = 1337;
      await expect(llamaPurchase.connect(GovernanceContract).deleteToken(id))
        .to.emit(llamaPurchase, "TokensDeleted")
        .withArgs(id, 0);
    });
    it("should allow others to donate their token (transfer) so long as the governance contract has set it up", async () => {
      const id = await llamaToken.getNextTokenId();
      const quantity = 500;
      await llamaToken
        .connect(GovernanceContract)
        .newItem([UserA.address], [], quantity);
      await expect(
        llamaPurchase.connect(GovernanceContract).addToken(id, 1000, 500)
      ).to.not.be.reverted;
      await expect(
        llamaToken
          .connect(UserA)
          .safeTransferFrom(
            UserA.address,
            llamaPurchase.address,
            id,
            quantity,
            []
          )
      )
        .to.emit(llamaPurchase, "TokensAdded")
        .withArgs(id, quantity);
    });
    it("should not allow others to donate their tokens (transfer) if the token was not setup", async () => {
      const id = await llamaToken.getNextTokenId();
      const quantity = 500;
      await llamaToken
        .connect(GovernanceContract)
        .newItem([UserA.address], [], quantity);
      await expect(
        llamaToken
          .connect(UserA)
          .safeTransferFrom(
            UserA.address,
            llamaPurchase.address,
            id,
            quantity,
            []
          )
      ).to.be.revertedWith("Token is not setup");
    });
    it("should not allow others to donate their tokens (transfer) after the token setup was deleted by the governance contract", async () => {
      const id = await llamaToken.getNextTokenId();
      const quantity = 500;
      await llamaToken
        .connect(GovernanceContract)
        .newItem([UserA.address], [], quantity);
      await expect(
        llamaPurchase.connect(GovernanceContract).addToken(id, 1000, 500)
      ).to.not.be.reverted;
      await expect(llamaPurchase.connect(GovernanceContract).deleteToken(id)).to
        .not.be.reverted;
      await expect(
        llamaToken
          .connect(UserA)
          .safeTransferFrom(
            UserA.address,
            llamaPurchase.address,
            id,
            quantity,
            []
          )
      ).to.be.revertedWith("Token is not setup");
    });
    it("should allow others to donate their token (batch transfer) so long as the governance contract has set it up", async () => {
      for (const token of batchTokens) {
        await expect(
          llamaPurchase
            .connect(GovernanceContract)
            .addToken(token.id, token.purchasePrice, token.probability)
        ).to.not.be.reverted;
        await expect(
          llamaToken
            .connect(GovernanceContract)
            .newItem([UserA.address], [], token.quantity)
        ).to.not.be.reverted;
      }
      const tokenIds = batchTokens.map((token) => token.id);
      const tokenQuantity = batchTokens.map((token) => token.quantity);
      await expect(
        llamaToken
          .connect(UserA)
          .safeBatchTransferFrom(
            UserA.address,
            llamaPurchase.address,
            tokenIds,
            tokenQuantity,
            []
          )
      )
        .to.emit(llamaPurchase, "TokensAdded")
        .withArgs(tokenIds[0], tokenQuantity[0])
        .and.to.emit(llamaPurchase, "TokensAdded")
        .withArgs(tokenIds[1], tokenQuantity[1])
        .and.to.emit(llamaPurchase, "TokensAdded")
        .withArgs(tokenIds[2], tokenQuantity[2]); // Fail non-implemented unit tests
    });
    it("should not allow others to donate their tokens (batch transfer) for resale if the token was not setup", async () => {
      for (const token of batchTokens) {
        await expect(
          llamaToken
            .connect(GovernanceContract)
            .newItem([UserA.address], [], token.quantity)
        ).to.not.be.reverted;
      }
      const tokenIds = batchTokens.map((token) => token.id);
      const tokenQuantity = batchTokens.map((token) => token.quantity);
      await expect(
        llamaToken
          .connect(UserA)
          .safeBatchTransferFrom(
            UserA.address,
            llamaPurchase.address,
            tokenIds,
            tokenQuantity,
            []
          )
      ).to.be.revertedWith("Some tokens are not setup");
    });
    it("should not allow others to donate their tokens (batch transfer) after the token setup was deleted by the governance contract", async () => {
      for (const token of batchTokens) {
        await expect(
          llamaPurchase
            .connect(GovernanceContract)
            .addToken(token.id, token.purchasePrice, token.probability)
        ).to.not.be.reverted;
        await expect(
          llamaToken
            .connect(GovernanceContract)
            .newItem([UserA.address], [], token.quantity)
        ).to.not.be.reverted;
      }
      const tokenIds = batchTokens.map((token) => token.id);
      const tokenQuantity = batchTokens.map((token) => token.quantity);
      for (const token of batchTokens) {
        await llamaPurchase.connect(GovernanceContract).deleteToken(token.id);
      }
      await expect(
        llamaToken
          .connect(UserA)
          .safeBatchTransferFrom(
            UserA.address,
            llamaPurchase.address,
            tokenIds,
            tokenQuantity,
            []
          )
      ).to.be.revertedWith("Some tokens are not setup");
    });
    it("should not let someone other then the governance contract setup a token", async () => {
      const purchasePrice = 1000;
      const probability = 500;
      const id = await llamaToken.getNextTokenId();
      await expect(
        llamaPurchase.addToken(id, purchasePrice, probability)
      ).to.be.revertedWith("Caller not governance contract");
    });
    it("should not let someone other then the governance contract delete a token", async () => {
      const id = await llamaToken.getNextTokenId();
      const quantity = 50;
      await expect(
        llamaPurchase.connect(GovernanceContract).addToken(id, 1000, 500)
      ).to.not.be.reverted;
      await expect(
        llamaToken
          .connect(GovernanceContract)
          .newItem([llamaPurchase.address], [], quantity)
      )
        .to.emit(llamaPurchase, "TokensAdded")
        .withArgs(id, quantity);

      await expect(
        llamaPurchase.connect(UserA).deleteToken(id)
      ).to.be.revertedWith("Caller not governance contract");
    });
    it("should allow a user to create a player profile by donating to the DAO", async () => {
      const beforeDonationBalance = await GovernanceContract.getBalance();
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("2"));
      const afterDonationBalance = await GovernanceContract.getBalance();
      const blockEpoch = await llamaPurchase.blockEpoch();
      expect(afterDonationBalance.sub(beforeDonationBalance)).to.equal(
        parseEther("2")
      );
      const playerProfile = await llamaPurchase.players(UserA.address);
      expect(playerProfile.totalDonated).to.equal(parseEther("2"));
      expect(playerProfile.lastBlockRequested).to.equal(blockEpoch);
      expect(playerProfile.sequentialrequests).to.equal(0);
      expect(playerProfile.totalAwarded).to.equal(0);
    });
    it("should allow a user to donate more then the minimum when creating a player profile", async () => {
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("50") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("50"));
    });
    it("should not let a user donate less then the minimum when creating a player profile", async () => {
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("0.5") })
      ).to.be.revertedWith("Did not donate minimum");
    });
    it("should allow a new player profile to immediately call tipTheLLamas for the minimum balance", async () => {
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("2"));
      await expect(llamaPurchase.connect(UserA).tipTheLLamas())
        .to.emit(llamaPurchase, "LlamaTipped")
        .withArgs(UserA.address, 0, 50);
    });
    it("should not allow a user to create a new player profile if one has already been setup", async () => {
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("2"));
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      ).to.be.revertedWith("Account already created");
    });
    it("should allow the governance contract to remove a users player profile", async () => {
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("2"));
      await expect(
        llamaPurchase.connect(GovernanceContract).removePlayer(UserA.address)
      )
        .to.emit(llamaPurchase, "PlayerRemoved")
        .withArgs(UserA.address);
      const playerProfile = await llamaPurchase.players(UserA.address);
      expect(playerProfile.totalDonated).to.equal(0);
      expect(playerProfile.lastBlockRequested).to.equal(0);
      expect(playerProfile.sequentialrequests).to.equal(0);
      expect(playerProfile.totalAwarded).to.equal(0);
    });
    it("should allow a player to recreate their player profile after being removed by the DAO", async () => {
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("2"));
      await expect(
        llamaPurchase.connect(GovernanceContract).removePlayer(UserA.address)
      )
        .to.emit(llamaPurchase, "PlayerRemoved")
        .withArgs(UserA.address);
      let playerProfile = await llamaPurchase.players(UserA.address);
      expect(playerProfile.totalDonated).to.equal(0);
      expect(playerProfile.lastBlockRequested).to.equal(0);
      expect(playerProfile.sequentialrequests).to.equal(0);
      expect(playerProfile.totalAwarded).to.equal(0);
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("2"));
      playerProfile = await llamaPurchase.players(UserA.address);
      expect(playerProfile.totalDonated).to.equal(parseEther("2"));
    });
    it("should not allow other users from removing a users tokenRequest profile", async () => {
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("2"));
      await expect(
        llamaPurchase.connect(UserC).removePlayer(UserA.address)
      ).to.be.revertedWith("Caller not governance contract");
    });
    it("should allow the player to donate funds in any amount other then zero", async () => {
      await expect(
        llamaPurchase.connect(UserA).newPlayer({ value: parseEther("2") })
      )
        .to.emit(llamaPurchase, "NewPlayer")
        .withArgs(UserA.address, parseEther("2"));
      await expect(
        llamaPurchase.connect(UserA).donate({ value: 0 })
      ).to.be.revertedWith("No value sent");
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("0.00001") })
      )
        .to.emit(llamaPurchase, "PlayerDonation")
        .withArgs(UserA.address, parseEther("0.00001"), parseEther("2.00001"));
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("0.1") })
      )
        .to.emit(llamaPurchase, "PlayerDonation")
        .withArgs(UserA.address, parseEther("0.1"), parseEther("2.10001"));
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("1") })
      )
        .to.emit(llamaPurchase, "PlayerDonation")
        .withArgs(UserA.address, parseEther("1"), parseEther("3.10001"));
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("1.00001") })
      )
        .to.emit(llamaPurchase, "PlayerDonation")
        .withArgs(UserA.address, parseEther("1.00001"), parseEther("4.10002"));
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("10.00001") })
      )
        .to.emit(llamaPurchase, "PlayerDonation")
        .withArgs(
          UserA.address,
          parseEther("10.00001"),
          parseEther("14.10003")
        );
    });
    it("should not allow a user without a player profile to donate", async () => {
      await expect(
        llamaPurchase.connect(UserA).donate({ value: 0 })
      ).to.be.revertedWith("Player Profile has not been setup");
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("0.00001") })
      ).to.be.revertedWith("Player Profile has not been setup");
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("0.1") })
      ).to.be.revertedWith("Player Profile has not been setup");
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("1") })
      ).to.be.revertedWith("Player Profile has not been setup");
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("1.00001") })
      ).to.be.revertedWith("Player Profile has not been setup");
      await expect(
        llamaPurchase.connect(UserA).donate({ value: parseEther("10.00001") })
      ).to.be.revertedWith("Player Profile has not been setup");
    });
    it(
      "should allow the governance contract to slash all staked funds of an entropy provider INCLUDING pending withdraws"
    );
    it("should not allow others to slash staked funds of an entropy provider");
    it("should allow the governance contract to change the blockResetHeight", async () => {
      const newBlockHeight = 5_000;
      await expect(
        llamaPurchase
          .connect(GovernanceContract)
          .changeBlockResetHeight(newBlockHeight)
      )
        .to.emit(llamaPurchase, "NewBlockResetHeight")
        .withArgs(blockResetHeight, newBlockHeight);
      // Verify the blockResetHeight equals the new value AND that the new value is different then the old one (test assertion)
      expect(await llamaPurchase.blockResetHeight())
        .to.equal(newBlockHeight)
        .and.not.to.equal(blockResetHeight);
    });
    it("should not allow others to change the blockResetHeight", async () => {
      const newBlockHeight = 5_000;
      await expect(
        llamaPurchase.connect(UserA).changeBlockResetHeight(newBlockHeight)
      ).to.be.revertedWith("Caller not governance contract");
      // verify the blockResetHeight equals the old value AND that the old value is different from the new one (test assertion)
      expect(await llamaPurchase.blockResetHeight())
        .to.equal(blockResetHeight)
        .and.not.to.equal(newBlockHeight);
    });
    it("should allow the governance contract to change the override height", async () => {
      const newOverrideHeight = 5_000;
      await expect(
        llamaPurchase
          .connect(GovernanceContract)
          .changeOverrideHeight(newOverrideHeight)
      )
        .to.emit(llamaPurchase, "NewOverrideHeight")
        .withArgs(overrideHeight, newOverrideHeight);
      // Verify the blockResetHeight equals the new value AND that the new value is different then the old one (test assertion)
      expect(await llamaPurchase.overrideHeight())
        .to.equal(newOverrideHeight)
        .and.not.to.equal(overrideHeight);
    });
    it("should not allow others contract to change the override height", async () => {
      const newOverrideHeight = 5_000;
      await expect(
        llamaPurchase.connect(UserA).changeOverrideHeight(newOverrideHeight)
      ).to.be.revertedWith("Caller not governance contract");
      // verify the blockResetHeight equals the old value AND that the old value is different from the new one (test assertion)
      expect(await llamaPurchase.overrideHeight())
        .to.equal(overrideHeight)
        .and.not.to.equal(newOverrideHeight);
    });
  });
  describe("Entropy Provider Operations", () => {
    beforeEach(async () => {});
    it(
      "should allow an entropy provider to add digital commitments if they have enough staked tipped llamas"
    );
    it(
      "should not allow an entropy provider to add digital commitments if they dont have enough stake tipped llamas"
    );
    it(
      "should allow an entropy provider to pull out all but minimum stake in a time locked request"
    );
    it(
      "should allow an entropy provider to relieve themselves of their role and release all staked assets after a time locked request"
    );
    it(
      "should not allow others from relieving an entropy provider of their roles"
    );
    it("should not allow others from adding digital commitments");
    it(
      "should allow anyone with the digital commitment to complete a purchase order"
    );
    it(
      "should not allow an invalid digital commitment to complete a purchase order"
    );
    it(
      "should reward the entropy provider for processing purchase orders in a timely manner"
    );
    it(
      "should slash entropy providers if they fail to process orders in a timely manner"
    );
  });
  describe("User Operations", () => {
    beforeEach(async () => {});
    it(
      "should allow a user to request tipped llamas after they donate to the DAO"
    );
    it(
      "should not allow a user to request tipped llamas before donating to the DAO"
    );

    // Sanctioned users are users the DAO wishes to reset their free tipped llamas progress
    // and the sanction requires them to start over at the minimum request value plus another
    // native token donation. Perhaps if the user was using bots or otherwise doing questionable
    // things.
    it(
      "should not allow a user to request tipped llamas after being sanctioned by the DAO"
    );
    it(
      "should allow a sanctioned user to re-donate to the DAO to be to request tipped llamas"
    );
  });
  describe("Purchase Order Flow", () => {
    beforeEach(async () => {});
    it(
      "should not allow a user to request a purchase order for a non-setup token"
    );
    it(
      "should not allow a user to start a purchase order with zero stock available"
    );
    it(
      "should not allow a user to start a purchase order when all available stock is held in pending purchase orders"
    );
    it(
      "should not allow a users to start a purchase if they have insufficient funds"
    );
    it(
      "should allow a user to start a purchase if the tokens been setup and in stock"
    );
    it(
      "should allow an entropy provider to process a purchase a valid purchase order"
    );
    it(
      "should allow a user complete a purchase order thats been processed by a entropy provider"
    );
    it(
      "should not allow a user to request a purchase override if the block delay has not been met"
    );
    it(
      "should not allow a user to request a purchase override if the entropy provider has processed the purchase order"
    );
    it(
      "should allow a user to request a purchase order override if the order was not processed and the override height has been hit and should slash the entropy provider"
    );
    it(
      "should not allow a user to force a purchase order complete if they have not requested a purchase order override"
    );
    it(
      "should allow a user to force a purchase order complete after the request delay"
    );
    it(
      "should allow anyone to slash a users bond and receive the reporting bounty if they fail to complete their purchase order in a timely manner"
    );
  });
});
