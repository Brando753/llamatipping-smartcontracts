import {expect} from 'chai';
import { ethers } from "hardhat";
import { LlamaShares__factory } from "../typechain-types";
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';


describe("LlamaShares governance token constructor", () => {
    let userA: SignerWithAddress, userB: SignerWithAddress, userC: SignerWithAddress;
    let llamaSharesFactory: LlamaShares__factory;
    const invalidShareNumberError = "Total number of whole shares is not exactly 100";
    beforeEach(async () => {
        [userA, userB, userC] = await ethers.getSigners();
        llamaSharesFactory = await ethers.getContractFactory("LlamaShares");
    });
    it("should refuse to deploy more then 100 shares to a single user", async () => {
        const llamaShares = llamaSharesFactory.deploy([userA.address], [101e18.toString()]);
        await expect(llamaShares).to.be.revertedWith(invalidShareNumberError);
    });
    it("should refuse to deploy less then 100 shares to a single user", async () => {
        const llamaShares = llamaSharesFactory.deploy([userA.address], [99e18.toString()]);
        await expect(llamaShares).to.be.revertedWith(invalidShareNumberError);
    });
    it("should refuse to deploy more then 100 shares amongst all users", async () => {
        const llamaShares = llamaSharesFactory.deploy([userA.address, userB.address, userC.address], [33e18.toString(), 35e18.toString(), 33e18.toString()]);
        await expect(llamaShares).to.be.revertedWith(invalidShareNumberError);
    });
    it("should refuse to deploy less then 100 shares amongst all users", async () => {
        const llamaShares = llamaSharesFactory.deploy([userA.address, userB.address, userC.address], [33e18.toString(), 33e18.toString(), 33e18.toString()]);
        await expect(llamaShares).to.be.revertedWith(invalidShareNumberError);
    });
    it("should deploy 100 tokens to a single user", async () => {
        const llamaShares = await llamaSharesFactory.deploy([userA.address], [100e18.toString()]);
        expect(await llamaShares.balanceOf(userA.address)).to.equal(100e18.toString());
    });
    it("should deploy a total of 100 tokens amongst all users", async () => {
        const llamaShares = await llamaSharesFactory.deploy([userA.address, userB.address, userC.address], [33e18.toString(), 34e18.toString(), 33e18.toString()]);
        expect(await llamaShares.balanceOf(userA.address)).to.equal(33e18.toString());
        expect(await llamaShares.balanceOf(userB.address)).to.equal(34e18.toString());
        expect(await llamaShares.balanceOf(userC.address)).to.equal(33e18.toString());
    });
});