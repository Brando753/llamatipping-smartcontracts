import { expect } from 'chai';
import { ethers, upgrades } from "hardhat";
import { LlamaDirectory, LlamaDirectory__factory, LlamaDirectoryMockUpgrade, LlamaDirectoryMockUpgrade__factory } from "../typechain-types";
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { checkPrimeSync } from 'crypto';

describe("Llama Directory", () => {
    let llamaDirectoryFactory: LlamaDirectory__factory;
    let llamaDirectory: LlamaDirectory;
    let userA: SignerWithAddress, userB: SignerWithAddress, userC: SignerWithAddress, userD: SignerWithAddress;
    let purchaseContract: SignerWithAddress, governanceContract: SignerWithAddress;

    // Hash strings like in solidity
    const keccak256ToAddress = (input: string) => ethers.utils.getAddress(ethers.utils.keccak256(ethers.utils.toUtf8Bytes(input)).slice(0, -24));

    const governanceTokenAddress = keccak256ToAddress("GOVERNANCE_TOKEN");
    const llamaTokenAddress = keccak256ToAddress("LLAMA_TAKEN_ADDRESS");
    const llamaPurchaseAddress = keccak256ToAddress("LLAMA_PURCHASE_ADDRESS");
    const newLlamaContractAddress = keccak256ToAddress("NEW_LLAMA_CONTRACT_ADDRESS");

    const updatedGovernanceTokenAddress = keccak256ToAddress("GOVERNANCE_TOKEN_2");
    const updatedLlamaTokenAddress = keccak256ToAddress("LLAMA_TAKEN_ADDRESS_2");
    const updatedLlamaPurchaseAddress = keccak256ToAddress("LLAMA_PURCHASE_ADDRESS_2");
    const updatedNewLlamaContractAddress = keccak256ToAddress("NEW_LLAMA_CONTRACT_ADDRESS_2");

    async function checkLlamaDirectory() {
        expect(await llamaDirectory.getGovernanceContractAddress()).to.equal(governanceContract.address);
        expect(await llamaDirectory.getGovernanceTokenAddress()).to.equal(governanceTokenAddress);
        expect(await llamaDirectory.getLlamaTokenAddress()).to.equal(llamaTokenAddress);
        expect(await llamaDirectory.getLlamaPurchaseAddress()).to.equal(llamaPurchaseAddress);
        return;
    }

    async function checkUpdatedLlamaDirectory() {
        expect(await llamaDirectory.getGovernanceTokenAddress()).to.equal(updatedGovernanceTokenAddress);
        expect(await llamaDirectory.getLlamaTokenAddress()).to.equal(updatedLlamaTokenAddress);
        expect(await llamaDirectory.getLlamaPurchaseAddress()).to.equal(updatedLlamaPurchaseAddress);
        return;
    }

    beforeEach(async () => {
        [userA, userB, userC, userD, purchaseContract, governanceContract] = await ethers.getSigners();
        llamaDirectoryFactory = await ethers.getContractFactory("LlamaDirectory");
    });

    describe("Standard Deploy", () => {
        beforeEach(async () => {
            llamaDirectory = await llamaDirectoryFactory.deploy();
            await llamaDirectory.initialize(
                governanceTokenAddress,
                llamaTokenAddress,
                governanceContract.address,
                llamaPurchaseAddress
            );
        });
        it("should properly deploy the smart contract and call the initialize method.", async () => {
            await checkLlamaDirectory();
        });
        it("should not allow the llama directory to be updated by someone other then the governance contract", async () => {
            await checkLlamaDirectory();
            const invalidUsers = [userA, userB, userC, userD, purchaseContract];
            for (const user of invalidUsers) {
                await expect(llamaDirectory.connect(user).setAddresses(
                    updatedGovernanceTokenAddress,
                    updatedLlamaTokenAddress,
                    user.address,
                    updatedLlamaPurchaseAddress
                )).to.be.revertedWith("Ownable: caller is not the owner");
            }
            await checkLlamaDirectory();
        });
        it("should allow the llama directory to be updated by the governance contract address to new addresses", async () => {
            await checkLlamaDirectory();
            await expect(llamaDirectory.connect(governanceContract).setAddresses(
                updatedGovernanceTokenAddress,
                updatedLlamaTokenAddress,
                governanceContract.address,
                updatedLlamaPurchaseAddress
            )).to.not.be.reverted;
            await checkUpdatedLlamaDirectory();
            expect(await llamaDirectory.getGovernanceContractAddress()).to.equal(governanceContract.address);
        });
        it("should reassign ownership when the governance contract address changes", async () => {
            await checkLlamaDirectory();
            await llamaDirectory.connect(governanceContract).setAddresses(
                updatedGovernanceTokenAddress,
                updatedLlamaTokenAddress,
                userA.address,
                updatedLlamaPurchaseAddress
            );
            await checkUpdatedLlamaDirectory();
            expect(await llamaDirectory.getGovernanceContractAddress()).to.equal(userA.address);
            await expect(llamaDirectory.connect(userA).setAddresses(
                governanceTokenAddress,
                llamaTokenAddress,
                governanceContract.address,
                llamaPurchaseAddress
            )).to.not.be.reverted;
            await checkLlamaDirectory();
        });
    });

    describe("Proxy Deploy", () => {
        let llamaDirectoryMockUpgradeFactory: LlamaDirectoryMockUpgrade__factory;
        beforeEach(async () => {
            llamaDirectory = await upgrades.deployProxy(llamaDirectoryFactory, [
                governanceTokenAddress,
                llamaTokenAddress,
                governanceContract.address,
                llamaPurchaseAddress
            ], { initializer: "initialize" }) as LlamaDirectory;
            llamaDirectoryMockUpgradeFactory = await ethers.getContractFactory("LlamaDirectoryMockUpgrade");
        });
        it("should properly deploy the smart contract and call the initialize method.", async () => {
            await checkLlamaDirectory();
        });
        it("should not allow the llama directory to be updated by someone other then the governance contract", async () => {
            await checkLlamaDirectory();
            const invalidUsers = [userA, userB, userC, userD, purchaseContract];
            for (const user of invalidUsers) {
                await expect(llamaDirectory.connect(user).setAddresses(
                    updatedGovernanceTokenAddress,
                    updatedLlamaTokenAddress,
                    user.address,
                    updatedLlamaPurchaseAddress
                )).to.be.revertedWith("Ownable: caller is not the owner");
            }
            await checkLlamaDirectory();
        });
        it("should allow the llama directory to be updated by the governance contract address to new addresses", async () => {
            await checkLlamaDirectory();
            await expect(llamaDirectory.connect(governanceContract).setAddresses(
                updatedGovernanceTokenAddress,
                updatedLlamaTokenAddress,
                governanceContract.address,
                updatedLlamaPurchaseAddress
            )).to.not.be.reverted;
            await checkUpdatedLlamaDirectory();
            expect(await llamaDirectory.getGovernanceContractAddress()).to.equal(governanceContract.address);
        });
        it("should reassign ownership when the governance contract address changes", async () => {
            await checkLlamaDirectory();
            await llamaDirectory.connect(governanceContract).setAddresses(
                updatedGovernanceTokenAddress,
                updatedLlamaTokenAddress,
                userA.address,
                updatedLlamaPurchaseAddress
            );
            await checkUpdatedLlamaDirectory();
            expect(await llamaDirectory.getGovernanceContractAddress()).to.equal(userA.address);
            await expect(llamaDirectory.connect(userA).setAddresses(
                governanceTokenAddress,
                llamaTokenAddress,
                governanceContract.address,
                llamaPurchaseAddress
            )).to.not.be.reverted;
            await checkLlamaDirectory();
        });
        it("should allow us to upgrade the llama directory contract to a new contract with a new getter available", async () => {
            await checkLlamaDirectory();
            const newLlama = await upgrades.upgradeProxy(llamaDirectory, llamaDirectoryMockUpgradeFactory);
            await checkLlamaDirectory();
            expect(newLlama.address).to.equal(llamaDirectory.address);
        });
        it("should allow us to upgrade and then set the new variable by setAddresses call", async () => {
            await checkLlamaDirectory();
            const newLlama = await upgrades.upgradeProxy(llamaDirectory, llamaDirectoryMockUpgradeFactory) as LlamaDirectoryMockUpgrade;
            await checkLlamaDirectory();
            await expect(newLlama.connect(governanceContract).setAddresses(
                governanceTokenAddress,
                llamaTokenAddress,
                governanceContract.address,
                llamaPurchaseAddress,
                newLlamaContractAddress
            )).to.not.be.reverted;
            await checkLlamaDirectory();
            expect(await newLlama.getNewLLamaContractAddress()).to.equal(newLlamaContractAddress);
        });
        it("should allow us to upgrade and then reassign ownership on the new upgraded contract when governance contract changes", async () => {
            await checkLlamaDirectory();
            const newLlama = await upgrades.upgradeProxy(llamaDirectory, llamaDirectoryMockUpgradeFactory) as LlamaDirectoryMockUpgrade;
            await checkLlamaDirectory();
            await expect(newLlama.connect(governanceContract).setAddresses(
                governanceTokenAddress,
                llamaTokenAddress,
                userA.address,
                llamaPurchaseAddress,
                newLlamaContractAddress
            )).to.not.be.reverted;
            await expect(newLlama.connect(userA).setAddresses(
                updatedGovernanceTokenAddress,
                updatedLlamaTokenAddress,
                governanceContract.address,
                updatedLlamaPurchaseAddress,
                updatedNewLlamaContractAddress
            )).to.not.be.reverted;
            await checkUpdatedLlamaDirectory();
            expect(await newLlama.getNewLLamaContractAddress()).to.equal(updatedNewLlamaContractAddress);
        });
    });
});