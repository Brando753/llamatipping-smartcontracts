import { CompilerOutputContract } from "hardhat/types/artifacts"

export interface StorageSlot {
    astId: number;
    contract: string;
    label: string;
    offset: number;
    slot: string;
    type: string;
}

export interface StorageLayout {
    storage: StorageSlot[];
    types: unknown;
}

export interface CompilerOutputContractWithSlots extends CompilerOutputContract {
    storageLayout: StorageLayout;
}

export const NULL_ADDRESS = "0x0000000000000000000000000000000000000000"