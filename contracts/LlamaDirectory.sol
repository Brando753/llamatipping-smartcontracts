//SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";


contract LlamaDirectory is OwnableUpgradeable {
  // Contract address for the governance token
  address private _governanceToken;
  // Contract address for the governance contract
  address private _governanceContract;
  // Contract address for the llama token 
  address private _llamaToken;
  // Contract address for the llama purchase contract
  address private _llamaPurchase;

  
  function initialize(address governanceTokenAddress, address llamaTokenAddress, address governanceContractAddress, address llamaPurchase) public initializer {
    __Ownable_init();
    setAddresses(governanceTokenAddress, llamaTokenAddress, governanceContractAddress, llamaPurchase);
  }

  function setAddresses(address governanceTokenAddress, address llamaTokenAddress, address governanceContractAddress, address llamaPurchase) public onlyOwner {
      _governanceToken = governanceTokenAddress;
      _llamaToken = llamaTokenAddress;
      _governanceContract = governanceContractAddress;
      _llamaPurchase = llamaPurchase;
      _transferOwnership(governanceContractAddress);
  }

  function getGovernanceTokenAddress() public view returns (address) {
        return _governanceToken;
    }

  function getLlamaTokenAddress() public view returns (address) {
    return _llamaToken;
  }

  function getGovernanceContractAddress() public view returns (address) {
    return _governanceContract;
  }

  function getLlamaPurchaseAddress() public view returns (address) {
    return _llamaPurchase;
  }
}
