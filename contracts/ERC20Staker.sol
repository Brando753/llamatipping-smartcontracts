// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Votes.sol";

abstract contract ERC20Staker is ERC20 {
  /**
   * @notice The period in seconds that a user must wait to unstake tokens by requesting an unbonding
   */
  uint256 public immutable unbondingPeriod;

  /**
   * @dev User Records for how much is staked, awaiting unstaking (unbonding), timelock user must
   * wait to unstake, and total tokens awaiting unstaking and staked.
   * @custom:member stakedTokens the quantity of tokens a user has staked
   * @custom:member unbondingTokens the quantity of previously staked tokens a user is requesting to unstake
   * @custom:member unbondingLockOutTimeStamp the time stamp at which point a user can unstake all tokens in the unbondingTokens count
   * @custom:member unavailableTokens the total quantity of tokens that are staked and pending unstaking (stakedTokens + unbondingTokens)
   */
  struct StakerRecords {
    uint256 stakedTokens;
    uint256 unbondingTokens;
    uint256 unbondingLockOutTimeStamp;
    uint256 unavailableTokens;
  }

  /**
   * @dev User addresses to their staking records for whats available to slash or unstake
   */
  mapping(address => StakerRecords) public staker;

  /**
   * @notice Error is thrown when a function attempts to stake, unstake, or otherwise spend more
   * tokens then available for that function. You can only unstake as many tokens as you have
   * staked, and you can only stake as many tokens as your total token balance is.
   */
  error ExceededBalance(uint256 balance, uint256 requestedAmount);

  /**
   * @notice Error is thrown when an address is expected but the null address is given. This
   * function will not accept the null address as a valid account.
   */
  error InvalidAddress();

  /**
   * @notice Error is thrown when a function expects a non-zero quantity either for staking or unbonding.
   */
  error ExpectedNonZeroQuantity();
  /**
   * @notice An attempt was made to slash more tokens then the user has staked
   * (both staked and awaiting unstaking) and so the request was denied
   */
  error ExcessiveSlash(uint256 atStake, uint256 requstedSlash);

  /**
   * @notice An attempt was made to unstake tokens when currently no tokens are at stake for the user
   */
  error NoUnbondingRequest();

  /**
   * @notice An attempt was made to unstake tokens before the required time period had passed
   */
  error TimeLockOut(
    uint256 currentTimeStamp,
    uint256 unbondingLockoutTimeStamp
  );

  /**
   * @notice Event emitted whenever a user increases the amount of tokens they are willing to stake
   * @param staker the address of the user putting their tokens at stake
   * @param quantityStaked the quantity they have decided to stake with this request
   * @param totalStaked the total amount they now have staked with the contract
   */
  event TokenStaked(
    address indexed staker,
    uint256 quantityStaked,
    uint256 totalStaked
  );

  /**
   * @notice Event emitted whenever a user requests a token unbonding so that they can unstake their
   * tokens at a later time. This event does not mean the tokens are unstaked, merely that they can
   * be unstaked after the time lock.
   * @param staker The account which is requesting the token unbonding
   * @param quantityUnbonded The amount the user is requesting to unbond
   * @param remainingStaked The amount which the user would leave staked
   * @param unbondingDate The timestamp for which a user will be able to unstake these tokens
   */
  event TokenUnbonding(
    address indexed staker,
    uint256 quantityUnbonded,
    uint256 remainingStaked,
    uint256 unbondingDate
  );

  /**
   * @notice Event emitted whenever a user completes an unbonding time period and unstakes their tokens.
   * These tokens are no longer at stake from slashing and can be transferred like normal.
   * @param staker The account which is requesting the token unbonding
   * @param quantityUnstaked The amount of tokens which have been unstaked
   * @param remainingStaked The amount of tokens the user still has staked with the contract
   */
  event TokenUnstaked(
    address indexed staker,
    uint256 quantityUnstaked,
    uint256 remainingStaked
  );

  /**
   * @notice Event emitted whenever a user is slashed against staked tokens. Upon this event staked tokens
   * followed by unbonding tokens depending upon the slash quantity will be depleted and immediately burned.
   * @param SlashedAddress The account which was slashed in this event
   * @param Slasher The account which ordered the slashing event
   * @param quantitySlashed The amount of tokens the user was slashed for
   * @param remainingStaked The amount of tokens the user has remaining staked
   * @param remainingUnbonding The amount of tokens the user has remaining unbonding
   */
  event TokenSlashed(
    address indexed SlashedAddress,
    address indexed Slasher,
    uint256 quantitySlashed,
    uint256 remainingStaked,
    uint256 remainingUnbonding
  );

  /**
   * @dev Modifier that checks the provided address is not invalid. Currently only checks that it
   * is not the Zero (NULL) address.
   * @param account The account to check
   * @custom:error Reverts with error InvalidAddress if test fails
   */
  modifier ValidAddress(address account) {
    if (account == address(0)) {
      revert InvalidAddress();
    }
    _;
  }

  /**
   * @dev Modifier that checks the provided quantity is not zero.
   * @param quantity The quantity to check is not zero
   * @custom:error Reverts with error ExpectedNonZeroQuantity if the quantity is zero
   */
  modifier NonZeroQuantity(uint256 quantity) {
    if (quantity == 0) {
      revert ExpectedNonZeroQuantity();
    }
    _;
  }

  /**
   * @notice Constructor sets the unbondingPeriod which is how long a user must wait before
   * they can unstake their tokens.
   * @dev unbondingPeriod is an immutable variable, it can never be changed so be sure to
   * pick a sensible value
   *
   * @param _unbondingPeriod The value to wait in seconds between unbonding and unstaking
   */
  constructor(uint256 _unbondingPeriod) {
    unbondingPeriod = _unbondingPeriod;
  }

  /**
   * @dev A modifier to check that the requested transfer or burn amount is available from the
   * pool of available tokens. Will revert if available token pool has insufficient balance.
   *
   * @param account The account to check a valid transfer amount on
   * @param amount The amount to check the balance against
   * @custom:errer Will revert with ExceededBalance if the amount exeeds what the
   * total `balance - unavailable` balance is
   */
  modifier StakedTokenTransfer(address account, uint256 amount) {
    uint256 quantityUnavailable = staker[account].unavailableTokens;
    uint256 availableBalance = this.balanceOf(account) - quantityUnavailable;

    if (amount > availableBalance) {
      revert ExceededBalance(availableBalance, amount);
    }
    _;
  }

  /**
   * @dev Overridden _transfer function prevents a user from transferring tokens when they are
   * staked or unbonding. Only tokens not locked in the unavailable balance should be transferable.
   * @param from The account transferring the tokens
   * @param to The account receiving the tokens
   * @param amount The amount of tokens being transferred
   * @custom:error Will revert with ExceededBalance if the transfer amount exceeds what the
   * total `balance - unavailable` balance is
   */
  function _transfer(
    address from,
    address to,
    uint256 amount
  ) internal virtual override StakedTokenTransfer(from, amount) {
    super._transfer(from, to, amount);
  }

  /**
   * @dev Overridden _burn function prevents a user from burning tokens when they are
   * staked or unbonding. Only tokens not locked in the unavailable balance should be burnable.
   * @param account The account burning the tokens
   * @param amount The amount of tokens being burned
   * @custom:error Will revert with ExceededBalance if the burn amount exceeds what the
   * total `balance - unavailable` balance is
   */
  function _burn(address account, uint256 amount)
    internal
    virtual
    override
    StakedTokenTransfer(account, amount)
  {
    super._burn(account, amount);
  }

  /**
   * @notice External function that allows a token holder to stake up to the balance they hold
   * in tokens. Staked tokens can be slashed by the token contract and the functions it provides
   * with calls to _slash but staked tokens are tracked and can be used by the system to grant
   * privileges or trusted features. While tokes are staked they cannot be transferred to other
   * accounts.
   * @param stakingQuantity The quantity of tokens the user wishes to stake
   * @dev Staking is an instant action, immediately after calling this function the users
   * tokens will be staked
   * @custom:error Will revert with ExceededBalance if the amount they attempt to stake exceeds their total token balance
   * @custom:error Will revert with ExpectedNonZeroQuantity if the stakedQuantity is zero
   */
  function stake(uint256 stakingQuantity) external {
    _stake(msg.sender, stakingQuantity);
  }

  /**
   * @dev A invariance checker that insures no records where lost with the staking/unbonding/unstaking
   * actions and that no coins where created or lost. These assertions should NEVER fail, if they do
   * there is a bug in the code. Call this whenever you update the users StakerRecord.
   * @param user The users StakerRecord we are checking before writing
   * @param userAddress The address of the user we are checking
   * @custom:error Will revert transaction with a failed assertion using up all available gas
   */
  function _assertStaker(StakerRecords memory user, address userAddress)
    private
    view
  {
    assert(user.stakedTokens + user.unbondingTokens == user.unavailableTokens);
    assert(this.balanceOf(userAddress) >= user.unavailableTokens);
  }

  /**
   * @notice Internal function for inheriting contracts to call when they want to slash a users funds.
   * Must be a non-zero address and must be slashing a quantity greater then zero but less then or equal
   * to the users staked assets.
   * @dev Tokens that can be slashed are staked tokens as well as unbonding tokens that have not passed the
   * timelock period.
   * @param account The user we are slashing
   * @param slashedQuantity The amount we should slash from the user
   * @custom:error Will revert with InvalidAddress if the account is invalid (the null address)
   * @custom:error Will revert with ExpectedNonZeroQuantity if the slashedQuantity is zero
   * @custom:error Will revert with ExcessiveSlash if the slashedQuantity exceeds the users total staked and unbonding quantities
   */
  function _slash(address account, uint256 slashedQuantity)
    internal
    virtual
    ValidAddress(account)
    NonZeroQuantity(slashedQuantity)
  {
    StakerRecords memory user = staker[account];

    _beforeTokenSlash(account, slashedQuantity, user);

    if (slashedQuantity > user.unavailableTokens) {
      revert ExcessiveSlash(user.unavailableTokens, slashedQuantity);
    }

    // We want to slash from the users staked tokens first and then any extra against their
    // unbonding tokens next
    if (slashedQuantity > user.stakedTokens) {
      unchecked {
        uint256 unbondedSlashed = slashedQuantity - user.stakedTokens;
        user.stakedTokens = 0;
        user.unbondingTokens -= unbondedSlashed;
      }
    } else {
      unchecked {
        user.stakedTokens -= slashedQuantity;
      }
    }
    unchecked {
      user.unavailableTokens -= slashedQuantity;
    }
    _assertStaker(user, account);
    staker[account] = user;

    // Actually burn the tokens
    super._burn(account, slashedQuantity);

    emit TokenSlashed(
      account,
      msg.sender,
      slashedQuantity,
      user.stakedTokens,
      user.unbondingTokens
    );

    _afterTokenSlash(account, slashedQuantity, user);
  }

  /**
   * @dev Internal function that inheriting contracts can call to stake balances for an account.
   * Requires the account to have the balance before letting them stake. Each call attempts to stake
   * the stakingQuantity in addition to any tokens already staked.
   * @param account The account that is staking their tokens
   * @param stakingQuantity The amount the account will be staking
   * @custom:error Will revert with ExceededBalance if the amount they attempt to stake exceeds their total token balance
   * @custom:error Will revert with InvalidAddress if the account is invalid (the null address)
   * @custom:error Will revert with ExpectedNonZeroQuantity if the stakedQuantity is zero
   */
  function _stake(address account, uint256 stakingQuantity)
    internal
    virtual
    ValidAddress(account)
    NonZeroQuantity(stakingQuantity)
  {
    StakerRecords memory user = staker[account];

    _beforeTokenStake(account, stakingQuantity, user);

    uint256 accountBalance = this.balanceOf(account);
    // Can overflow if staking quantity + unavailableTokens exceeds 256 bits, will revert automatically
    uint256 requestingAmount = stakingQuantity + user.unavailableTokens;

    if (accountBalance < requestingAmount) {
      revert ExceededBalance(accountBalance, stakingQuantity);
    }

    unchecked {
      user.stakedTokens = user.stakedTokens + stakingQuantity;
      user.unavailableTokens = user.unavailableTokens + stakingQuantity;
    }

    _assertStaker(user, account);
    staker[account] = user;

    emit TokenStaked(account, stakingQuantity, user.stakedTokens);

    _afterTokenStake(account, stakingQuantity, user);
  }

  /**
   * @notice External function that allows a token holder to unstake there unbonded tokens after the timelock period
   * has ended. This function will unstake all their unbonded tokens rather then ask for a quantity.
   * @dev Unstaking is an instant action, immediately after calling this function the users
   * tokens will be unStaked HOWEVER they must have first called unbond and waited the time-locked period before
   * calling this function.
   * @custom:error Will revert with TimeLockOut if the time-locked period has not passed
   * @custom:error Will revert with NoUnbondingRequest if unbond was not first called
   */
  function unstake() external {
    _unstake(msg.sender);
  }

  /**
   * @dev Internal function that inheriting contracts can call to unstake all balances for the tokens the account holder has
   * unbonded.
   * @param account The account that is unstaking their tokens
   * @custom:error Will revert with InvalidAddress if the account is invalid (the null address)
   * @custom:error Will revert with TimeLockOut if the time-locked period has not passed
   * @custom:error Will revert with NoUnbondingRequest if unbond was not first called
   */
  function _unstake(address account) internal virtual ValidAddress(account) {
    StakerRecords memory user = staker[account];

    _beforeTokenUnstake(account, user);

    if (block.timestamp < user.unbondingLockOutTimeStamp) {
      revert TimeLockOut(block.timestamp, user.unbondingLockOutTimeStamp);
    }

    if (user.unbondingLockOutTimeStamp == 0 || user.unbondingTokens == 0) {
      revert NoUnbondingRequest();
    }

    uint256 unstakedAmount = user.unbondingTokens;
    unchecked {
      user.unavailableTokens -= unstakedAmount;
      user.unbondingTokens = 0;
      // We could possibly remove this to make next unstake cheaper but setting
      // But setting to zero should give us a gas refund making it a minor optimization
      // If not set to zero we must thoroughly test there is no way to rapidly unstake
      user.unbondingLockOutTimeStamp = 0;
    }
    _assertStaker(user, account);

    staker[account] = user;
    emit TokenUnstaked(account, unstakedAmount, user.stakedTokens);

    _afterTokenUnstake(account, user);
  }

  /**
   * @notice External function for starting the unbonding period of staked tokens. User must request how much they want
   * to unbond and then wait the time locked period to unstake. If a user submits an unbond call while a previous unbonding
   * request was sent the newer one overwrites the old one and the timelock period restarts. If a quantity of zero is provided
   * the pending unbond request will be canceled.
   * @param unbondingQuantity The quantity of staked assets the user wants to start unbonding
   * @custom:error Will revert with ExceededBalance if attempting to unbond more tokens then staked + unbonding
   */
  function unbond(uint256 unbondingQuantity) external {
    // A public request to set the unbond amount to zero is a request to cancel the unbonding
    if (unbondingQuantity == 0) {
      _cancelUnbond(msg.sender);
    } else {
      _unbond(msg.sender, unbondingQuantity);
    }
  }

  /**
   * @dev Internal function for canceling pending unbond requests. Given a user account this function will reset any unbonding tokens
   * back to staked tokens and then reset the lock out timestamp back to zero.
   * @param account The address of the account to cancel the pending unbond request of
   * @custom:error Will revert with InvalidAddress if the address is the ZERO (NULL) address
   */
  function _cancelUnbond(address account)
    internal
    virtual
    ValidAddress(account)
  {
    StakerRecords memory user = staker[account];

    _beforeTokenUnbonding(account, 0, user);

    user.stakedTokens += user.unbondingTokens;
    user.unbondingTokens = 0;
    user.unbondingLockOutTimeStamp = 0;
    _assertStaker(user, account);
    staker[account] = user;

    emit TokenUnbonding(
      account,
      0,
      user.stakedTokens,
      user.unbondingLockOutTimeStamp
    );

    _afterTokenUnbonding(account, 0, user);
  }

  /**
   * @dev Internal function for starting the unbonding process of users staked tokens. Account and address are
   * submitted and so long the user has sufficiently bonded tokens (staked or awaiting unbond) it will begin the
   * timelocked unbonding process. If a user has a pending unbond request this new one will override the last
   * request.
   * @param account The account to initiate an unbond request from
   * @param unbondingQuantity The amount to unbond thats currently at stake
   * @custom:error Will revert with ExpectedNonZeroQuantity if quantity of unbonding is zero
   * @custom:error Will revert with ExceededBalance if attempting to unbond more tokens then staked + unbonding
   * @custom:error Will revert with InvalidAddress if the address is the ZERO (NULL) address
   */
  function _unbond(address account, uint256 unbondingQuantity)
    internal
    virtual
    ValidAddress(account)
    NonZeroQuantity(unbondingQuantity)
  {
    StakerRecords memory user = staker[account];

    _beforeTokenUnbonding(account, unbondingQuantity, user);

    if (unbondingQuantity > user.unavailableTokens) {
      revert ExceededBalance(user.unavailableTokens, unbondingQuantity);
    }
    unchecked {
      user.stakedTokens = user.unavailableTokens - unbondingQuantity;
      user.unbondingTokens = unbondingQuantity;
      user.unbondingLockOutTimeStamp = block.timestamp + unbondingPeriod;
    }
    _assertStaker(user, account);
    staker[account] = user;
    emit TokenUnbonding(
      account,
      unbondingQuantity,
      user.stakedTokens,
      user.unbondingLockOutTimeStamp
    );

    _afterTokenUnbonding(account, unbondingQuantity, user);
  }

  /**
   * @dev Hook that is called before token slashing occurs.
   *
   * @param account The Users address/account that is about to be slashed
   * @param amount The amount of tokens that is about to be slashed
   * @param user The Users StakerRecords with the current balances BEFORE slashing occurs
   **/
  function _beforeTokenSlash(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal virtual {}

  /**
   * @dev Hook that is called after token slashing has occurred
   * @param account The Users address/account that has just been slashed
   * @param amount  The amount that user has been slashed for
   * @param user The Users StakerRecords with the current balances AFTER slashing has occurred
   */
  function _afterTokenSlash(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal virtual {}

  /**
   * @dev Hook that is called before a user attempts to stake any tokens
   * @param account The Users address/account that has initiated a stake request
   * @param amount  The amount that user has requested to staked
   * @param user The Users StakerRecords with the current balances BEFORE staking has occurred
   */
  function _beforeTokenStake(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal virtual {}

  /**
   * @dev Hook that is called after a user attempts to stake any tokens
   * @param account The Users address/account that has initiated a stake request
   * @param amount  The amount that user has staked
   * @param user The Users StakerRecords with the current balances AFTER staking has occurred
   */
  function _afterTokenStake(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal virtual {}

  /**
   * @dev Hook that is called before a user attempts to unbond any tokens
   * @param account The Users address/account that has initiated a unbond request
   * @param amount  The amount that user has requested to be unbonded
   * @param user The Users StakerRecords with the current balances before the unbond request has occurred
   */
  function _beforeTokenUnbonding(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal virtual {}

  /**
   * @dev Hook that is called after a user attempts to unbond any tokens
   * @param account The Users address/account that has initiated a unbond request
   * @param amount  The amount that user has requested be unbonded
   * @param user The Users StakerRecords with the current balances AFTER unbond request has occurred
   */
  function _afterTokenUnbonding(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal virtual {}

  /**
   * @dev Hook that is called before a user attempts to unstake any tokens
   * @param account The Users address/account that has initiated the unstake request
   * @param user The Users StakerRecords with the current balances before un\staking has occurred
   */
  function _beforeTokenUnstake(address account, StakerRecords memory user)
    internal
    virtual
  {}

  /**
   * @dev Hook that is called after a user attempts to unstake any tokens
   * @param account The Users address/account that has initiated the unstake request
   * @param user The Users StakerRecords with the current balances after unstaking has occurred
   */
  function _afterTokenUnstake(address account, StakerRecords memory user)
    internal
    virtual
  {}
}
