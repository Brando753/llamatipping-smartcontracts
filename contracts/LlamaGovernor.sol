// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/governance/Governor.sol";
import "@openzeppelin/contracts/governance/extensions/GovernorProposalThreshold.sol";
import "@openzeppelin/contracts/governance/extensions/GovernorCountingSimple.sol";
import "@openzeppelin/contracts/governance/extensions/GovernorVotes.sol";
import "@openzeppelin/contracts/governance/extensions/GovernorVotesQuorumFraction.sol";
import "./LlamaShares.sol";

contract LlamaGovernor is Governor, GovernorProposalThreshold, GovernorCountingSimple, GovernorVotes, GovernorVotesQuorumFraction {
    /**
     * @dev Governance contract for the Llama Tippers, allows users to vote on proposals and through voting
     *      manipulate the LlamaToken, the LlamaDirectory, and the LlamaPurchase contracts.
     * @param _token the ERC20 contract that the governance contract will use for voting
     */
    constructor(LlamaShares _token)
        Governor("LlamaGovernor")
        GovernorVotes(_token)
        GovernorVotesQuorumFraction(51)
    {}

    function votingDelay() public pure override returns (uint256) {
        return 216000; // 5 days at two second block heights
    }

    function votingPeriod() public pure override returns (uint256) {
        return 216000; // 5 days at two second block heights
    }

    function proposalThreshold() public pure override returns (uint256) {
        return 5e18;
    }

    // The following functions are overrides required by Solidity.

    function quorum(uint256 blockNumber)
        public
        view
        override(IGovernor, GovernorVotesQuorumFraction)
        returns (uint256)
    {
        return super.quorum(blockNumber);
    }

    // function getVotes(address account, uint256 blockNumber)
    //     public
    //     view
    //     override(IGovernor, GovernorVotes)
    //     returns (uint256)
    // {
    //     return super.getVotes(account, blockNumber);
    // }

    function propose(address[] memory targets, uint256[] memory values, bytes[] memory calldatas, string memory description)
        public
        override(Governor, GovernorProposalThreshold)
        returns (uint256)
    {
        return super.propose(targets, values, calldatas, description);
    }
}