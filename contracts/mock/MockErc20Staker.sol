// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "../ERC20Staker.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

contract MockStaker is ERC20Staker, ERC20Burnable {
  /**
   * @dev Simple mock contract that inherits ERC20Staker for unit test purposes
   * Deployer can call the mint function to issue tokens at inception
   */

  /**
   * @dev The owner of the contract (contract deployer) address
   */
  address owner;

  /**
   * @dev A tracker for hooks that ensure they occurred in the correct order
   */
  bool entryRan;

  /**
   * @dev Error raised when an entry was run out of order
   */
  error HooksOutOfOrder();

  /**
   * @dev Error raised when anyone other then the deployer calls the external mint function
   */
  error UserIsNotOwner();

  /**
   * @dev Simple modifier that prevents anyone other then contract deployer from accessing a function
   */
  modifier OnlyOwner() {
    if (msg.sender != owner) {
      revert UserIsNotOwner();
    }
    _;
  }

  /**
   * @dev Modifier for Before Hooks that ensures entryRan has not been set yet
   */
  modifier BeforeHook() {
    if (entryRan == true) {
      revert HooksOutOfOrder();
    }
    entryRan = true;
    _;
  }

  /**
   * @dev Modifier for After Hooks that ensures entryRan has been set
   */
  modifier AfterHook() {
    if (entryRan == false) {
      revert HooksOutOfOrder();
    }
    entryRan = false;
    _;
  }

  /**
   * @dev Deploys the contract with a set timelock for unstaking an initial minting issued to the deployer
   * and a Name of "Staker Delights" with a Symbol of "STDs"
   * @param timelock How long the contract should wait before letting a user unstake their tokens
   * @param quantity How many initial tokens should be minted to the deployer?
   */
  constructor(uint256 timelock, uint256 quantity)
    ERC20("Staker Delights", "STDs")
    ERC20Staker(timelock)
  {
    address deployer = msg.sender;
    owner = deployer;
    entryRan = false;
    _mint(deployer, quantity);
  }

  /**
   * @dev External function that can only be called by the deployer of the contract, allows the user to mint any number
   * of tokens to the address specified.
   * @param to The address to mint the tokens to
   * @param quantity The quantity of tokens to mint
   * @custom:error Will revert with UserIsNotOwner if anyone other then contract deployer calls this function
   */
  function mint(address to, uint256 quantity) external OnlyOwner {
    _mint(to, quantity);
  }

  /**
   * @dev External function that can only be called by the deployer of the contract, allows the user to slash anyone's staked token.
   * This gives access to the internal function _slash for unit testing
   * @param victim The address to slash
   * @param quantity The amount of tokens to slash
   * @custom:error Will revert with UserIsNotOwner if anyone other then contract deployer calls this function
   * @custom:error Will revert with InvalidAddress if the victim account is invalid (the null address)
   * @custom:error Will revert with ExpectedNonZeroQuantity if the quantity is zero
   * @custom:error Will revert with ExcessiveSlash if the slashedQuantity exceeds the users total staked and unbonding quantities
   */
  function slash(address victim, uint256 quantity) external OnlyOwner {
    _slash(victim, quantity);
  }

  /**
   * @dev External function that can only be called by the deployer of the contract, allows the user to call the internal _stake function for unit testing
   * @param account The account that is staking their tokens
   * @param stakingQuantity The amount the account will be staking
   * @custom:error Will revert with UserIsNotOwner if anyone other then contract deployer calls this function
   * @custom:error Will revert with ExceededBalance if the amount they attempt to stake exceeds their total token balance
   * @custom:error Will revert with InvalidAddress if the account is invalid (the null address)
   * @custom:error Will revert with ExpectedNonZeroQuantity if the stakedQuantity is zero
   */
  function __stake(address account, uint256 stakingQuantity)
    external
    OnlyOwner
  {
    _stake(account, stakingQuantity);
  }

  /**
   * @dev External function that can only be called by the deployer of the contract, allows the user to call the internal _unstake function for unit testing
   * @param account The account that is unstaking their tokens
   * @custom:error Will revert with UserIsNotOwner if anyone other then contract deployer calls this function
   * @custom:error Will revert with InvalidAddress if the account is invalid (the null address)
   * @custom:error Will revert with TimeLockOut if the time-locked period has not passed
   * @custom:error Will revert with NoUnbondingRequest if unbond was not first called
   */
  function __unstake(address account) external OnlyOwner {
    _unstake(account);
  }

  /**
   * @dev External function that can only be called by the deployer of the contract, allows the user to call the internal _unbond function for unit testing
   * @param account The account to initiate an unbond request from
   * @param unbondingQuantity The amount to unbond thats currently at stake
   * @custom:error Will revert with UserIsNotOwner if anyone other then contract deployer calls this function
   * @custom:error Will revert with ExpectedNonZeroQuantity if quantity of unbonding is zero
   * @custom:error Will revert with ExceededBalance if attempting to unbond more tokens then staked + unbonding
   * @custom:error Will revert with InvalidAddress if the address is the ZERO (NULL) address
   */
  function __unbond(address account, uint256 unbondingQuantity)
    external
    OnlyOwner
  {
    _unbond(account, unbondingQuantity);
  }

  /**
   * @dev External function that can only be called by the deployer of the contract, allows the user to call the internal _cancelUnbond function for unit testing
   * @param account The account to cancel/reset the unbond request from
   * @custom:error Will revert with UserIsNotOwner if anyone other then contract deployer calls this function
   * @custom:error Will revert with InvalidAddress if the address is the ZERO (NULL) address
   */
  function __cancelUnbond(address account) external OnlyOwner {
    _cancelUnbond(account);
  }

  /**
   * @dev External function that can only be called by the deployer to selfdestruct the contract.
   * @custom:error Will revert with UserIsNotOwner if anyone other then the contract deployer calls this function
   */
  function selfDestruct() external OnlyOwner {
    address payable _owner = payable(owner);
    selfdestruct(_owner);
  }

  /**
   * @dev Overload the  _burn functions to test this contract as a burnable
   */
  function _burn(address account, uint256 amount)
    internal
    virtual
    override(ERC20Staker, ERC20)
  {
    super._burn(account, amount);
  }

  /**
   * @dev Overload the _transfer function to test this contract as burnable
   */
  function _transfer(
    address from,
    address to,
    uint256 amount
  ) internal virtual override(ERC20Staker, ERC20) {
    super._transfer(from, to, amount);
  }

  /**
   * @dev Override hooks so that we can test they are working and in correct order
   */
  event BeforeTokenSlash(address account, uint256 amount, StakerRecords user);
  event AfterTokenSlash(address account, uint256 amount, StakerRecords user);
  event BeforeTokenStake(address account, uint256 amount, StakerRecords user);
  event AfterTokenStake(address account, uint256 amount, StakerRecords user);
  event BeforeTokenUnbonding(
    address account,
    uint256 amount,
    StakerRecords user
  );
  event AfterTokenUnbonding(
    address account,
    uint256 amount,
    StakerRecords user
  );
  event BeforeTokenUnstake(address account, StakerRecords user);
  event AfterTokenUnstake(address account, StakerRecords user);

  function _beforeTokenSlash(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal override BeforeHook {
    emit BeforeTokenSlash(account, amount, user);
  }

  function _afterTokenSlash(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal override AfterHook {
    emit AfterTokenSlash(account, amount, user);
  }

  function _beforeTokenStake(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal override BeforeHook {
    emit BeforeTokenStake(account, amount, user);
  }

  function _afterTokenStake(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal override AfterHook {
    emit AfterTokenStake(account, amount, user);
  }

  function _beforeTokenUnbonding(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal override BeforeHook {
    emit BeforeTokenUnbonding(account, amount, user);
  }

  function _afterTokenUnbonding(
    address account,
    uint256 amount,
    StakerRecords memory user
  ) internal override AfterHook {
    emit AfterTokenUnbonding(account, amount, user);
  }

  function _beforeTokenUnstake(address account, StakerRecords memory user)
    internal
    override
    BeforeHook
  {
    emit BeforeTokenUnstake(account, user);
  }

  function _afterTokenUnstake(address account, StakerRecords memory user)
    internal
    override
    AfterHook
  {
    emit AfterTokenUnstake(account, user);
  }
}
