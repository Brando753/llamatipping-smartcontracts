// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Burnable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";

contract LlamaToken is
  ERC1155,
  ERC1155Supply,
  ERC1155Burnable,
  AccessControlEnumerable
{
  using Counters for Counters.Counter;
  Counters.Counter private _tokenIds;

  // Track the owner of the MAGIC_CONCH_SHELL so that the governance contract can change owners
  address private conchOwner;

  // Create unique roles for privlaged authorizations
  bytes32 public constant URI_ROLE = keccak256("URI_ROLE");
  bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
  bytes32 public constant LLAMA_MINTER_ROLE = keccak256("LLAMA_MINTER_ROLE");

  // Max quantity of Tipped Llamas
  uint256 public constant TIPPED_LLAMAS_CAP = 100_000_000;

  // Main Llama-tipper currency
  uint256 public constant TIPPED_LLAMAS = 0;
  // Current Llama-Tipper Leadership Badge
  uint256 public constant MAGIC_CONCH_SHELL = 1;
  // Llama Tipper address that participated in the Mumbai testnet
  uint256 public constant GLITCHY_LLAMA = 2;
  // Llama-Tipper at the time of this contracts launch
  uint256 public constant OLD_LLAMA = 3;

  // Setup the initial Llama Tipper Badges
  /**
   * @dev Constructor which sets up the LLamaToken 1155 contract
   * @param tippedAddresses List of addresses which are being given TippedLLamas on construction
   * @param testerAddress List of addresses that supported the initial DevNet test and get a GLITCHY_LLAMA badge for it
   * @param tippedQuantity Quantity of TippedLLamas associated tippedAddress recieves, indexes must match
   * @param _conchOwner Address that is recieving the Magic Conch Shell NFT which signifies the groups leader
   * @param governanceContract Address of the Llama Tippers Governance Contract
   * @param purchaseContract Address of the Llama Tippers Llama Purchase Contract to buy Badges and acquire tipped llamas
   */
  constructor(
    address[] memory tippedAddresses,
    address[] memory testerAddress,
    uint256[] memory tippedQuantity,
    address _conchOwner,
    address governanceContract,
    address purchaseContract
  ) ERC1155("https://llama-tipping.info/tokens/{id}.json") {
    require(
      tippedAddresses.length == tippedQuantity.length,
      "Quantity and Addresses arrays not matching"
    );

    // Setup Role based authenticatcion, default admin is governance contract
    _setupRole(DEFAULT_ADMIN_ROLE, governanceContract);
    _setupRole(URI_ROLE, governanceContract);
    _setupRole(MINTER_ROLE, governanceContract);
    _setupRole(LLAMA_MINTER_ROLE, governanceContract);
    _setupRole(LLAMA_MINTER_ROLE, purchaseContract);

    conchOwner = _conchOwner;
    _mint(_conchOwner, MAGIC_CONCH_SHELL, 1, "");
    for (uint256 index = 0; index < testerAddress.length; ++index) {
      _mint(testerAddress[index], GLITCHY_LLAMA, 1, "");
    }

    // TODO: REMOVE THIS SECTION IN PRODUCTION
    _mint(purchaseContract, GLITCHY_LLAMA, 5000, "");

    // Creation of OLD_LLAMA Badge in LOOP
    for (uint256 index = 0; index < tippedAddresses.length; ++index) {
      _mint(tippedAddresses[index], TIPPED_LLAMAS, tippedQuantity[index], "");
      _mint(tippedAddresses[index], OLD_LLAMA, 1, "");
    }
    // Index 0 for Tipped Llamas
    _tokenIds.increment(); // Increment for Magic Conch Shell
    _tokenIds.increment(); // Increment for Glitchy Llama
    _tokenIds.increment(); // Increment for Old Llama
    require(
      totalSupply(TIPPED_LLAMAS) <= TIPPED_LLAMAS_CAP,
      "Supply of Tipped Llamas Is Not One Million Llamas or less"
    );
    assert(totalSupply(MAGIC_CONCH_SHELL) == 1);
  }

  /**
   */
  function getNextTokenId() external view returns (uint256) {
    return _tokenIds.current() + 1;
  }

  /**
   * @dev The governance contract may force a change of leadership by voting to change who may hold the Magic Conch Shell
   * @param newOwner the address of the new holder of the Magic Conch Shell
   */
  function changeConchOwnership(address newOwner) public {
    require(
      hasRole(DEFAULT_ADMIN_ROLE, msg.sender),
      "caller is not governance contract"
    );
    _safeTransferFrom(conchOwner, newOwner, MAGIC_CONCH_SHELL, 1, "");
    conchOwner = newOwner;
  }

  /**
   * @dev Before Token Transfer hook that checks if the holder of the magic conch shell attempts to transfer it to someone else
   *      if they do, record the new holder here.
   * @param operator The address requesting the transfer (either the token owner or a party approved by the owner)
   * @param from The address the transfer is coming from
   * @param to The address the transfer is going to
   * @param ids The id's being transfered from the user; array of one for single transfer
   * @param amounts The amount being transfered from the user; array of one for single transfer
   * @param data Data being sent along with the transfer per ERC1155 spec (UNUSED IN THIS CONTRACT)
   */
  function _beforeTokenTransfer(
    address operator,
    address from,
    address to,
    uint256[] memory ids,
    uint256[] memory amounts,
    bytes memory data
  ) internal virtual override(ERC1155, ERC1155Supply) {
    super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    if (from == conchOwner) {
      for (uint256 i = 0; i < ids.length; ++i) {
        if (ids[i] == MAGIC_CONCH_SHELL) {
          conchOwner = to;
        }
      }
    }
  }

  /**
   * @dev Token to mint more Tipped Llamas by llama minter, this function cannot mint more then the fixed cap set on construction.
   * @param reciever The address that is recieving the newly minted tipped llamas
   * @param quantity The quantity of tipped llamas to mint
   */
  function mintTippedLLamas(address reciever, uint256 quantity) external {
    require(
      hasRole(LLAMA_MINTER_ROLE, msg.sender),
      "Caller is not llama minter"
    );
    require(
      (totalSupply(TIPPED_LLAMAS) + quantity) <= TIPPED_LLAMAS_CAP,
      "Minting would exceed max supply"
    );
    _mint(reciever, TIPPED_LLAMAS, quantity, "");
  }

  /**
   * @dev Mint a one time custom badge or NFT, must have minting authority by the MINTER_ROLE
   * @param recievers[] The list of addresses to recieve the newly minted badge or NFT
   * @param data specify data to be passed with the token (UNUSED IN THIS CONTRACT)
   * @param quantity The quantity of badges to be minted to each address, same for all users
   */
  function newItem(
    address[] memory recievers,
    bytes memory data,
    uint256 quantity
  ) public returns (uint256) {
    require(hasRole(MINTER_ROLE, msg.sender), "Caller is not minter");
    require(recievers.length >= 1, "Must have at least one receiving address");
    require(quantity >= 1, "Must have at least quantity of one minted");
    _tokenIds.increment();
    uint256 newItemId = _tokenIds.current();
    for (uint256 index = 0; index < recievers.length; index++) {
      _mint(recievers[index], newItemId, quantity, data);
    }
    return newItemId;
  }

  /**
   * @dev Updates the URI for the token that can be used to lookup token metadata, only the governance contract can change this
   * @param newURI The new URI to use for token metadata lookup
   */
  function updateURI(string memory newURI) public {
    require(hasRole(URI_ROLE, msg.sender), "Caller is not authorized");
    _setURI(newURI);
  }

  /** Overloaded Functions to satisfy multiple inheritance problems with Solidity */
  // function _burn(address account, uint256 id, uint256 amount)
  //   internal
  //   override(ERC1155, ERC1155Supply)
  // {
  //   super._burn(account, id, amount);
  // }

  // function _burnBatch(address account, uint256[] memory ids, uint256[] memory amounts)
  //   internal
  //   override(ERC1155, ERC1155Supply)
  // {
  //   super._burnBatch(account, ids, amounts);
  // }

  // function _mint(address account, uint256 id, uint256 amount, bytes memory data)
  //   internal
  //   override(ERC1155, ERC1155Supply)
  // {
  //   super._mint(account, id, amount, data);
  // }

  // function _mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data)
  //   internal
  //   override(ERC1155, ERC1155Supply)
  // {
  //   super._mintBatch(to, ids, amounts, data);
  // }

  function supportsInterface(bytes4 interfaceId)
    public
    view
    override(AccessControlEnumerable, ERC1155)
    returns (bool)
  {
    return super.supportsInterface(interfaceId);
  }
}
