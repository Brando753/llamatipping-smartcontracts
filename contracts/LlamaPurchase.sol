// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./LlamaShares.sol";
import "./LlamaToken.sol";
import "hardhat/console.sol";


contract LlamaPurchase is ERC1155Holder, AccessControlEnumerable {
    // Contract interface for LlamaToken
    LlamaToken private llamaToken;

    // Contract interface for Governance Token
    LlamaShares private llamaShares;
    
    // Roles for Governance contract and entropy providers
    bytes32 public constant ENTROPY_PROVIDER_ROLE = keccak256("ENTROPY_PROVIDER_ROLE");
    bytes32 public constant GOVERNANCE_CONTRACT_ROLE = keccak256("GOVERNANCE_CONTRACT_ROLE");
    
    // The first block the contract is deployed on, for determining days of interaction
    uint256 public blockEpoch;
    
    // Number of blocks between days
    uint256 public blockResetHeight;
    
    // Array of digital commitments that entropy providers provide for generating random numbers
    bytes32[] public entropyPool;

    // Number of blocks a user has to wait before they can skip needing to use an entropy provider who is non-responsive
    uint256 public overrideHeight;

    // Minimum donation to become a new player
    uint256 public minimumDonation;

    // Address of the governance contract
    address payable public governanceContract;
    
    // Counter used for incrementing the purchase order numbers
    using Counters for Counters.Counter;
    Counters.Counter public purchaseNumber;

    // Struct for tracking each users free tipped llama requests, they can request once a day and gain more tipped llamas
    // for each sequential day they request in a row
    struct PlayerAccount {
        uint256 totalDonated; // Total amount this user has donated to the DAO
        uint256 lastBlockRequested; // Last block a request was made at
        uint256 sequentialrequests; // How many days in a row last request was made at
        uint256 totalAwarded; // How much total has been awarded to user through this service
    }

    // Tracking which NFT's / Badges / Tokens are stored in this contract for distribution to purchasers
    // Tipped Llamas is not tracked here as that is a special token which can be minted by this contract
    // These tokens are only transferable by the contract and must be owned by the contract
    struct TokenBalances {
        bool accepting; // Has this contract been setup to accept this token? Token must be setup before you can transfer to this contract
        uint256 tokenId; // ID of the token in the llama token contract
        uint256 lotteryPrice; // Price to purchase a lottery ticket to attempt to buy this token
        uint256 probability; // Probability of winning this token
        uint256 balance; // How many tokens the contract has available not sold or in pending purchases
    }

    struct EntropyBond {
        uint256 shareValues;
        
    }

    // Tracking pending user purchases in the system that need to be resolved. These can either be resolved by an entropy provider who reveals
    // their digital commitment or by the user themselves after submitting an override request for a non-responsive entropy provider. There
    // will be a minimum block wait before overrides are available and then the weaker past 20 block hashes will be used over a commited 
    // entropy provider.
    struct PurchaseOrder {
        uint256 purchaseOrderNumber;
        address purchaser;
        uint256 tokenId;
        uint256 randomEntropy;
        bytes32 digitalCommitment;
        uint256 blockHeight;
        uint256 overrideHeight;
    }

    event NewBlockResetHeight (
        uint256 indexed oldBlockHeight,
        uint256 indexed newBlockHeight
    );

    event NewOverrideHeight (
        uint256 indexed oldOverrideHeight,
        uint256 indexed newOverrideHeight
    );

    // Event emitted whenever a purchase order is made, will index on the order number, the address of the purchaser, and the token being purchased.
    event PurchaseMade (
        uint256 indexed purchaseOrderNumber,
        address indexed purchaser,
        uint256 indexed tokenId,
        uint256 randomEntropy,
        bytes32 digitalCommitment
    );

    event PlayerDonation (
        address indexed player,
        uint256 indexed donation,
        uint256 indexed totalDonations
    );

    event NewPlayer (
        address indexed player,
        uint256 donation
    );

    event PlayerRemoved (
        address indexed player
    );

    event TokensAdded (
        uint256 indexed tokenId,
        uint256 quantity
    );

    event TokensDeleted (
        uint256 indexed tokenId,
        uint256 quantityBurned
    );

    // Event emitted whenever a user requests free llama tokens successfully
    event LlamaTipped (
        address indexed user,
        uint256 consecutiveDays,
        uint256 awardedTokens
    );

    // Array of pending purchase orders, dApps should sort this list and find the index they intend to interact with before submitting an order.
    PurchaseOrder[] public pendingPurchaseOrders;

    // Mapping of token balances by id, this stores the token balance, the token price, probability of win, etc
    mapping (uint256 => TokenBalances)  public tokenBalances;

    // This is a mapping of override requests by purchaser
    mapping (address => PurchaseOrder[]) public overrideRequests;
    
    // This is a mapping of free token requetst by address of requester
    mapping (address => PlayerAccount) public players;

    // This is a mapping of Staked llamas bonded by Entropy Providers
    mapping (address => uint256) public StakedLlamas;


    /**
     * @dev This contract is not upgradabale. It provides purchase capabilities of llama tipping badges and NFT's along with a free faucet for tippedLlamas 
     *      if available. 
     * @param entropyProviders The default set of addresses that will act as entropy providers who submits digital commitments and finalizes standard purchase flow
     * @param _governanceContract The address of the governance contract that controls this contract
     * @param llamaTokenAddress The address of the llama tipping ERC1155 contract that mints the tokens, badges, and NFT's
     * @param _overrideHeight The number of blocks a user must wait if they want to override a purchase order due to a non-responsive entropy provider
     * @param _blockResetHeight Number of blocks between days on this network
     * @param _minimumDonation The minimum ammount of Matic a user must donate to become a player
     */
    constructor (address[] memory entropyProviders, address payable _governanceContract, address llamaTokenAddress, address llamaSharesAddress, uint256 _overrideHeight, uint256 _blockResetHeight, uint256 _minimumDonation) {
        uint256 entropyProvidersLength = entropyProviders.length;
        for(uint256 i; i<entropyProvidersLength;) {
            _setupRole(ENTROPY_PROVIDER_ROLE, entropyProviders[i]);
            unchecked {++i;}
        }
        _setupRole(DEFAULT_ADMIN_ROLE, _governanceContract);
        _setupRole(GOVERNANCE_CONTRACT_ROLE, _governanceContract);
        llamaShares = LlamaShares(llamaSharesAddress);
        llamaToken = LlamaToken(llamaTokenAddress);
        overrideHeight = _overrideHeight;
        blockEpoch = block.number-_blockResetHeight;
        blockResetHeight = _blockResetHeight;
        minimumDonation = _minimumDonation;
        governanceContract = _governanceContract;
    }

    // Modifier which checks if msg.sender of the transaction has the ENTROPY_PROVIDER_ROLE
    modifier isEntropyProvider {
        require(hasRole(ENTROPY_PROVIDER_ROLE, msg.sender), "Caller not authorized to provide entropy");
        _;
    }

    // Modifier which restricts operations to the governance contract
    modifier isGovernanceContract {
        require(hasRole(GOVERNANCE_CONTRACT_ROLE, msg.sender), "Caller not governance contract");
        _;
    }

    /**
     * @dev This function provides entropy providers a way to batch submit entropy digital commitments that users can pick from
     *      for a later reveal stage. 
     * @param entropyData an array of keccak256 hashes which correspond to the random number picked by the entropy provider
     */
    function addEntropy(bytes32[] calldata entropyData) external isEntropyProvider {
        for (uint256 i=0; i<entropyData.length; ++i) {
            entropyPool.push(entropyData[i]);
        }
    }

    /**
     * @dev Allows the governance contract to modify the blockResetHeight value for future interactions
     * @param newBlockResetHeight the new block height used to count days on the blockchain from blockEpoch
     * @custom:emits Emits a NewBlockResetHeight event notifying users of the change
     */
    function changeBlockResetHeight(uint256 newBlockResetHeight) external isGovernanceContract {
        uint256 oldBlockHeight = blockResetHeight;
        blockResetHeight = newBlockResetHeight;
        emit NewBlockResetHeight(oldBlockHeight, newBlockResetHeight);
    }

    /**
     * @dev Allows the governance contract to modify the overrideHeight value for future interactions
     * @param newOverrideHeight the new block height used to track when a user can bypass an entropy provider
     * @custom:emits Emits a NewOverrideHeight event notifying users of the change
     */
    function changeOverrideHeight(uint256 newOverrideHeight) external isGovernanceContract {
        uint256 oldOverrideHeight = overrideHeight;
        overrideHeight = newOverrideHeight;
        emit NewOverrideHeight(oldOverrideHeight, newOverrideHeight);
    }

    /**
     * @dev Players who have donated to the DAO can play for free tipped llamas however if the DAO bans or removes
     *      the player they will have to redonate to get a new account. Only the governance contract can call this.
     * @param user The address of the user to remove.
     * @custom:emits Emits PlayerRemoved log event with the removed players address    
     */
    function removePlayer(address user) external isGovernanceContract {
        delete players[user];
        emit PlayerRemoved(user);
    }

    /**
     * @dev Allows an address to donate Matic to the DAO to create a player account
     * @custom:emits Emits a NewPlayer log event with the new players info
     */
    function newPlayer() external payable {
        uint256 value = msg.value;
        address player = msg.sender;
        require(value >= minimumDonation, "Did not donate minimum");
        require(players[player].totalDonated == 0, "Account already created");
        governanceContract.transfer(value);
        players[player] = PlayerAccount(value, blockEpoch, 0, 0);
        emit NewPlayer(player, value);
    }

    /**
     * @dev Allows a user to donate to the DAO and have it recorded to their player profile
     * @custom:emits Emits a player donated event
     */
    function donate() external payable {
        address player = msg.sender;
        uint256 donations = players[player].totalDonated;
        require(donations > 0, "Player Profile has not been setup");
        uint256 value = msg.value;
        require(value > 0, "No value sent");
        donations += value;
        governanceContract.transfer(value);
        players[player].totalDonated = donations;
        emit PlayerDonation(player, value, donations);
    }

    /**
     * @dev This function allows the governance contract to setup a badge/token/nft for sale at a certain ticket price
     *      with a specific probablity of success. This must be called before any of the token, badge, or NFT is transfereed
     *      or else the transactions will fail.
     * @param tokenId The id of the token being setup
     * @param lotteryPrice The price of a ticket to get a chance to buy the token
     * @param probability The probability of winning this item
     */
    function addToken(uint256 tokenId, uint256 lotteryPrice, uint256 probability) public isGovernanceContract {
        tokenBalances[tokenId] = TokenBalances(true, tokenId, lotteryPrice, probability, 0);
    }

    /**
     * @dev This function allows a governance contract to delete a badge/token/nft that has been setup for allowing purchases.
     *      If the contract owns any of the asset it burns it.
     * @param tokenId The id of the token being deleted
     */
    function deleteToken(uint256 tokenId) public isGovernanceContract {
        delete tokenBalances[tokenId];
        uint256 balance = llamaToken.balanceOf(address(this), tokenId);
        llamaToken.burn(address(this), tokenId, balance);
        emit TokensDeleted(tokenId, balance);
    }

    /**
     * @dev This function allows a user to purchase a badge/token/nft using their balance of TippedLlama tokens. It creates a 
     *      purchase order that an entropy provider must complete. If an entropy provider fails to complete the order the user
     *      can override the purchase and force it through using the less secure hashing of block hashes.
     * @param badgeId The id of the token/badge/NFT that is being purchased
     * @param randomEntropy A random number generated by the user which will be xored against the entropy providers digital commitment
     *                      and the hash of the block hash to ensure that the generated number is truly random so long one of the parties
     *                      is acting in good faith.
     * TODO: Use an enumerable mapping for the purchase orders to not require later functions to know the index since the index can change
     *       between transactions of different users.
     */
    function purchaseBadge(uint256 badgeId, uint256 randomEntropy) external {
        // Check that we have a balance of the requested tokenId
        uint256 purchaseOrderNumber = purchaseNumber.current();
        TokenBalances memory token = tokenBalances[badgeId];
        require(token.balance > 0, "No badges of that Id available for sale");
        // Regardless if user wins the token, until the purchase order is complete reserve the token
        tokenBalances[badgeId].balance -= 1;
        // Burn their money
        llamaToken.burn(msg.sender, badgeId, token.lotteryPrice);
        bytes32 digitalCommitment = entropyPool[entropyPool.length - 1];
        entropyPool.pop();
        PurchaseOrder memory purchaseOrder = PurchaseOrder(purchaseOrderNumber, msg.sender, badgeId, randomEntropy, digitalCommitment, block.number, 0);
        pendingPurchaseOrders.push(purchaseOrder);
        emit PurchaseMade(purchaseOrderNumber, msg.sender, badgeId, randomEntropy, digitalCommitment);
        purchaseNumber.increment();        
    }

    /**
     * @dev Private function that removes a purchase order at index provided from the pendingPurchaseOrders array
     * @param index the index of the pending purchase order in array
     */
    function _removePurchaseOrder(uint256 index) private {
            // Remove the purchase order from the pending order list
        pendingPurchaseOrders[index] = pendingPurchaseOrders[pendingPurchaseOrders.length - 1];
        pendingPurchaseOrders.pop();
    }

    /**
     * @dev Private function that removes an override request of the msg.sender. This is atomic because of the users account nonce.
     * @param index the index of the indvidual users override request list
     */
    function _removeOverrideRequest(uint256 index) private {
        overrideRequests[msg.sender][index] = overrideRequests[msg.sender][overrideRequests[msg.sender].length -1];
        overrideRequests[msg.sender].pop();
    }

    /**
     * @dev Function called by issuer of digital commitment for the purchase order. They are obligated to submit
     *      the opening secret to the commitment after a purchase order has picked their commitment. Anyone can 
     *      call this function so long as the opening secret is valid for the digital commitment.
     * @param index The index of the pending purchase order to mark as completed.
     * @param randomEntropy The random number generated by the entropy provider that matches the digital commitment picked by
     *                      the purchase order.  
     * TODO: Either remove the block hash to prevent a miner from influencing the purchase order result or split this into two
     *       transactions such that the miner hash is collected before the digital commitment reveal.
     */
    function completePurchaseOrder(uint256 index, uint256 randomEntropy) external {
        PurchaseOrder memory purchaseOrder = pendingPurchaseOrders[index];
        bytes32 hashedEntropy = keccak256(abi.encodePacked(randomEntropy));
        require(purchaseOrder.digitalCommitment == hashedEntropy, "Digital Commitment Invalid");
        // Check if user has pending override and delete it 
        for (uint256 i=0; i<overrideRequests[msg.sender].length; ++i) {
            if (overrideRequests[msg.sender][i].purchaseOrderNumber == purchaseOrder.purchaseOrderNumber) {
                _removePurchaseOrder(i);
                break;
            }
        }
        TokenBalances memory token = tokenBalances[purchaseOrder.tokenId];
        uint256 blockHash = uint256(keccak256(abi.encodePacked(blockhash(block.number - 1))));
        uint256 randomNumber = (purchaseOrder.randomEntropy ^ randomEntropy ^ blockHash) % 10_000;
        if (randomNumber <= token.probability) {
            // We have a winner, transfer the tokens over to them
            llamaToken.safeTransferFrom(address(this), purchaseOrder.purchaser, token.tokenId, 1, "");
        } else {
            // No winner today sorry :(
            // Unreserve the balance that was being held
            tokenBalances[purchaseOrder.tokenId].balance += 1;
        }
        _removePurchaseOrder(index);
    }

    /**
     * @dev If a user has not had their purchase order completed by the entropy provider, and the overrideHeight has been reached or exceeded
     *      then they can call this function to request an override. Requesting an override will allow them to force the purchase order to
     *      completion 20 blocks after seeking the override. The block hashes of the next 20 blocks become the source of entropy. This 
     *      method is provided as a way for users to still order assets if no entropy providers are available, or if the entropy provider
     *      has lost the opening secret for this purchase order, however it should be noted that the entropy provider should always be used
     *      before the overrideHeight is reached as a purchaser could collude with miners of the last block to not submit a block if it would
     *      result in a losing ticket.
     * @param index the index of the purchase order in the pending purchase order list
     * TODO: Do not let users request new purchase overrides as they could keep rejesting new overrides until one would be a winning ticket
     */
    function requestPurchaseOrderOverride(uint256 index) external {
        PurchaseOrder memory purchaseOrder = pendingPurchaseOrders[index];
        uint256 purchaseOrderDelay = block.number - purchaseOrder.blockHeight;
        require(purchaseOrderDelay >= overrideHeight, "Delay not met");
        for (uint256 i=0; i<overrideRequests[msg.sender].length; ++i) {
            if (overrideRequests[msg.sender][i].purchaseOrderNumber == purchaseOrder.purchaseOrderNumber) {
                if (block.number - overrideRequests[msg.sender][i].overrideHeight < 255) {
                    revert("Cannot request new override until old one expires");
                }
                _removeOverrideRequest(i);
                break;
            }
        }
        purchaseOrder.overrideHeight = block.number;
        overrideRequests[msg.sender].push(purchaseOrder);
    }

    /**
     * @dev After a user has submited their request to override a purchase order they must wait 20 blocks for the source of entropy. 
     *      They must submit the purchase order completion within 255 blocks of requesting the override so that the blocks can be
     *      hashed. If a user fails to force a purchase Order to completion after requesting an override the only way for the order
     *      to be fulfilled is if the entropy provider completes the order at a later date. Users CANNOT request new overrides or
     *      else they would be able to refuse to complete a purchase order if it would result in a losing ticket.
     * @param index the index of the pending purchase order in dispute
     */
    function forcePurchaseOrderComplete(uint256 index) external {
        PurchaseOrder memory purchaseOrder = pendingPurchaseOrders[index];
        PurchaseOrder memory purchaseOverride;
        bool overridePending = false;
        uint256 overrideIndex;
        for (uint256 i=0; i<overrideRequests[msg.sender].length; ++i) {
            if (overrideRequests[msg.sender][i].purchaseOrderNumber == purchaseOrder.purchaseOrderNumber) {
                overridePending = true;
                purchaseOverride = overrideRequests[msg.sender][i];
                overrideIndex = i;
            }
        }
        require(overridePending == true, "No pending overrides");
        require(block.number - purchaseOverride.overrideHeight > 20, "Must wait atleast 20 blocks from request");
        require(block.number - purchaseOverride.overrideHeight < 256, "Must force override within 255 blocks");
        TokenBalances memory token = tokenBalances[purchaseOrder.tokenId];
        uint256 randomNumber = purchaseOrder.randomEntropy;
        for (uint256 i=0; i<20; ++i) {
            randomNumber ^= uint256(keccak256(abi.encodePacked(blockhash(purchaseOverride.overrideHeight + i))));
        }
        randomNumber %= 10_000;
        if (randomNumber <= token.probability) {
            // We have a winner, transfer the tokens over to them
            llamaToken.safeTransferFrom(address(this), purchaseOrder.purchaser, token.tokenId, 1, "");
        } else {
            // No winner today
            // Unreserve the balance that was being held
            tokenBalances[purchaseOrder.tokenId].balance += 1;
        }
        _removePurchaseOrder(overrideIndex);
        _removePurchaseOrder(index);
    }

    /**
     * @dev Function which gives requesting users free tippedLlama tokens, and increases their daily allocation for contiuned interaction on the chain. 
     *      This function will revert in tippedllamas have reached their market cap until some tipped llamas are burned off.
     * TODO: Require a donation of MATIC to the governance contract (set by another function) before this feature can be used to prevent users from
     *       generating large number of addresses to steal all the tippedLlamas
     */
    function tipTheLLamas() external {
        PlayerAccount memory tokenRequest = players[msg.sender];
        console.log("(BlockNumber: %d - BlockEpoch: %d) / BlockResetHeight: %d", block.number, blockEpoch, blockResetHeight);
        // timePeriod is the number of days between this call and contract creation
        uint256 timePeriod = (block.number - blockEpoch) / blockResetHeight;
        console.log("TimePeriod: %d", timePeriod);
        console.log("(LastBlockRequested: %d - BlockEpoch: %d) / BlockResetHeight: %d", tokenRequest.lastBlockRequested, blockEpoch, blockResetHeight);
        // timpePeriodOfLastRequest in number of days between last call of this function and contract creation
        uint256 timePeriodOfLastRequest = (tokenRequest.lastBlockRequested - blockEpoch) / blockResetHeight;
        console.log("TimePeriodOfLastRequest: %d", timePeriodOfLastRequest);
        require(timePeriod != timePeriodOfLastRequest, "Can only tip llamas once a day!");
        // Default award amount for non-consecutive calls
        console.log("TimePeriod: %d - TimePeriodOfLastRequest: %d", timePeriod, timePeriodOfLastRequest);
        uint256 awardAmount;
        uint256 sequentialRequests = tokenRequest.sequentialrequests;
        if (timePeriod - timePeriodOfLastRequest == 1) {
            // This is a consecutive call increment the counter
            if (sequentialRequests == 0) {
                awardAmount = 50;
            } else if (sequentialRequests == 1) {
                awardAmount = 80;
            } else if (sequentialRequests == 2) {
                awardAmount = 100;
            } else if (sequentialRequests == 3) {
                awardAmount = 150;
            } else if (sequentialRequests == 4) {
                awardAmount = 200;
            } else if (sequentialRequests == 5) {
                awardAmount = 250;
            } else {
                awardAmount = 300;
            }
            tokenRequest.sequentialrequests = sequentialRequests + 1;
        } else {
            // This was not a consecutive call
            sequentialRequests = 0;
            tokenRequest.sequentialrequests = sequentialRequests;
        }
        tokenRequest.lastBlockRequested = block.number;
        tokenRequest.totalAwarded += awardAmount;
        players[msg.sender] = tokenRequest;
        llamaToken.mintTippedLLamas(msg.sender, awardAmount);
        emit LlamaTipped(msg.sender, sequentialRequests, awardAmount);
    }

    /**
     * @dev This overriden function lets ERC1155 contracts know that this contract supports them. Will reject any contract other then the llamaToken 
     *      contract. Will revert if the token being sent over is not setup prior.
     * @param operator The account that is initiating the transfer
     * @param from The account that the tokens are coming from
     * @param id The id of the token being transfered
     * @param value The quantity of tokens being transfered
     * @param data Any extra data sent with the transfer
     */
    function onERC1155Received(address operator, address from, uint256 id, uint256 value, bytes memory data) public override returns (bytes4) {
        require(msg.sender == address(llamaToken), "Only can accept llama tokens");
        require(tokenBalances[id].accepting == true, "Token is not setup");
        require(value != 0, "Must send some token balance");
        tokenBalances[id].balance += value;
        emit TokensAdded(id, value);
        return super.onERC1155Received(operator, from, id, value, data);
    }

    /**
     * @dev This overriden function lets ERC1155 contracts know that this contract supports them. Will reject any contract other then the llamaToken
     *      contract. Will revert if any of the tokens being sent over have not been setup prior.
     * @param operator The account that is initiating the transfer
     * @param from The account that the tokens are coming from 
     * @param id An array of tokens being transfered, each id must have alread been setup prior.
     * @param value The quantity of tokens being transfered matching the index of the id array
     * @param data Any extra data sent with the transfer
     */
    function onERC1155BatchReceived(address operator, address from, uint256[] memory id, uint256[] memory value, bytes memory data) public override returns (bytes4) {
        require(msg.sender == address(llamaToken), "Only can accept llama tokens");
        for (uint256 i=0; i<id.length; ++i) {
            uint256 tokenId = id[i];
            uint256 tokenValue = value[i];
            require(tokenBalances[tokenId].accepting == true, "Some tokens are not setup");
            require(tokenValue != 0, "Some tokens did not send a token balance");
            tokenBalances[tokenId].balance += tokenValue;
            emit TokensAdded(tokenId, tokenValue);
        }
        return super.onERC1155BatchReceived(operator, from, id, value, data);
    }

    /**
     * OpenZepplin contract override to allow other contracts to verify the supported interfaces.
     */
    function supportsInterface(bytes4 interfaceId) public view override(AccessControlEnumerable, ERC1155Receiver) returns (bool) {
        return super.supportsInterface(interfaceId);
    }
}