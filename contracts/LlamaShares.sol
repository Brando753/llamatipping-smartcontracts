// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Votes.sol";

contract LlamaShares is ERC20, ERC20Permit, ERC20Votes {
    /**
     * @dev The llama tippers governance token, used for voting with the governance contract. There are a total of 
     *      100 shares each with 18 decimal places of percision, These are otherwise standard ERC20 tokens.
     * @param initialAddresses The addresses to send shares to on construction
     * @param initialQuantity The quantity of shares to send each address of the same index. Must add up to 100.
     */
    constructor(address[] memory initialAddresses, uint256[] memory initialQuantity) ERC20("LlamaShares", "LTS") ERC20Permit("LlamaShares") {
      for (uint256 index = 0; index < initialAddresses.length; index++) {
        _mint(initialAddresses[index], initialQuantity[index]);
      }
      // Verify that there is EXACTLY 100 shares of Llama
      require(totalSupply() == 100e18, "Total number of whole shares is not exactly 100");
    }

    function stake(uint256 quantity) external {

    }

    function unstake(uint256 quantity) external {
        
    }

    // The following functions are overrides required by Solidity.

    function _afterTokenTransfer(address from, address to, uint256 amount)
        internal
        override(ERC20, ERC20Votes)
    {
        super._afterTokenTransfer(from, to, amount);
    }

    function _mint(address to, uint256 amount)
        internal
        override(ERC20, ERC20Votes)
    {
        super._mint(to, amount);
    }

    function _burn(address account, uint256 amount)
        internal
        override(ERC20, ERC20Votes)
    {
        super._burn(account, amount);
    }
}